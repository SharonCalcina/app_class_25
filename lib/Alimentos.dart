import 'package:flutter/material.dart';

class Alimentos extends StatefulWidget {
  @override
  _AlimentosState createState() => _AlimentosState();
}

class _AlimentosState extends State<Alimentos> {
  List<String> image = [
    "arlequin.jpg",
    "bizcocho.jpg",
    "brazogitano.jpg",
    "brownie.jpg",
    "donas.jpg",
    "flancalabaza.jpg",
    "galletam.jpg",
    "ganaztes.jpg",
    "magchoc.jpg",
    "magdalena.jpg",
    "muffin.jpg",
    "pie.jpg",
    "queque.jpg"
  ];
  List<String> title = [
    "Arlequin",
    "Bizcocho de ahuyama",
    "Brazo Gitano",
    "Brownie de Castañas",
    "Donuts Caseras al Horno",
    "Flan de Calabaza",
    "Galleta de Mantequilla con Chocolate",
    "Gaznates dulces",
    "Magdalenas de Chocolate",
    "Magndalenas de Naranja y Yogur",
    "Muffins de calabaza",
    "Pie de Limon",
    "Queque Mamoleado"
  ];
  List<String> price = [
    "8",
    "15",
    "10",
    "12",
    "8",
    "6",
    "10",
    "5",
    "5",
    "5",
    "6",
    "12",
    "5"
  ];
  int total = 0;
  int index = 0;
  int quantity = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          title: Image.asset("assets/images/sc.png"),
          actions: [
            Container(
                height: 50,
                width: 40,
                margin: EdgeInsets.only(top: 2, bottom: 2, right: 20),
                child: Image.asset("assets/images/p.png")),
          ],
        ),
        body: SingleChildScrollView(
          child: Center(
            //width: 300,
            //color: Colors.amber,
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Image.asset(
                    "assets/images/${image[index]}",
                    width: 250,
                    height: 250,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  title[index],
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Precio ${price[index]} Bs",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.green,
                        ),
                        onPressed: () {
                          setState(() {
                            index -= 1;
                            if (index < 0) {
                              index = image.length - 1;
                            }
                          });
                        },
                        child: Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white,
                        )),
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.green,
                        ),
                        onPressed: () {
                          setState(() {
                            index += 1;
                            if (index > image.length - 1) {
                              index = 0;
                            }
                          });
                        },
                        child: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    button("Agregar"),
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.grey.shade800,
                        ),
                        onPressed: () {
                          setState(() {
                            if (quantity > 1) {
                              quantity -= 1;
                            }
                          });
                        },
                        child: Container(
                            padding: EdgeInsets.only(bottom: 5),
                            child: Icon(
                              Icons.minimize,
                              color: Colors.white,
                            ))),
                    Text(
                      quantity.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.grey.shade800,
                        ),
                        onPressed: () {
                          setState(() {
                            quantity += 1;
                          });
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
        ),
        bottomSheet: Container(
            padding: EdgeInsets.all(15),
            color: Colors.transparent.withOpacity(0.6),
            height: 60,
            width: MediaQuery.of(context).size.width,
            child: Text("Total a pagar :   $total Bs.",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold))));
  }

  Widget button(label) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(12),
        minimumSize: Size(20, 20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        backgroundColor: Colors.red,
      ),
      onPressed: () {
        setState(() {
          total += int.parse(price[index]) * quantity;
        });
      },
      child: Text(
        label,
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),
      /*label: Text(
          label,
          style: TextStyle(color: Colors.grey.shade800),
        )*/
    );
  }
}
