import 'package:flutter/material.dart';

class Calculadora extends StatefulWidget {
  @override
  _CalculadoraState createState() => _CalculadoraState();
}

class _CalculadoraState extends State<Calculadora> {
  TextEditingController monto = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora"),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.all(50),
                child: TextField(
                  controller: monto,
                  decoration: InputDecoration(
                      alignLabelWithHint: true, labelText: "Ingresa un monto"),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                button(Icons.add),
                button(Icons.minimize),
                button(Icons.close),
                button2("/")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                button3("1"),
                button3("2"),
                button3("3"),
                button2("C")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                button3("4"),
                button3("5"),
                button3("6"),
                button(Icons.backspace_outlined),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                button3("7"),
                button3("8"),
                button3("9"),
                button2("%"),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget button(icon) {
    return Container(
        width: 60,
        child: TextButton(
          style: TextButton.styleFrom(
            minimumSize: Size(20, 20),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            backgroundColor: Colors.green,
          ),
          onPressed: () {},
          child: Icon(
            icon,
            color: Colors.white,
          ),
          /*label: Text(
          label,
          style: TextStyle(color: Colors.grey.shade800),
        )*/
        ));
  }

  Widget button2(icon) {
    return Container(
        width: 60,
        child: TextButton(
          style: TextButton.styleFrom(
            minimumSize: Size(20, 20),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            backgroundColor: Colors.green,
          ),
          onPressed: () {},
          child: Text(
            icon,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ));
  }

  Widget button3(icon) {
    return Container(
        width: 60,
        child: TextButton(
          style: TextButton.styleFrom(
            minimumSize: Size(20, 20),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            backgroundColor: Colors.grey.shade800,
          ),
          onPressed: () {},
          child: Text(
            icon,
            style: TextStyle(color: Colors.white),
          ),
        ));
  }
}
