import 'dart:async';
import 'dart:math';
import 'package:app_class_25/Hamburguesas.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Detalle extends StatefulWidget {
  final List<Map<String, String>> listOfColumns;
  Detalle(this.listOfColumns);
  @override
  _DetalleState createState() => _DetalleState(this.listOfColumns);
}

class _DetalleState extends State<Detalle> {
  _DetalleState(this.listOfColumns);
  List<int> cantidad = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];
  List<Map<String, String>> listOfColumns;
  String data = "";
  String res;
  int total = 0;
  int nroFac = 0;
  _setData() async {
    print(listOfColumns.toString());

    SharedPreferences _prefs = await SharedPreferences.getInstance();
    Random r = new Random();

    int keys = r.nextInt(10000);
    while (_prefs.containsKey(keys.toString())) {
      keys = r.nextInt(10000);
    }
    String listP = listOfColumns.join('\n').toString().replaceAll("}", "");
    _prefs.setString(
        keys.toString(),
        "Fecha: ${DateTime.now().day}" +
            " - ${DateTime.now().month}" +
            " - ${DateTime.now().year}" +
            "                " +
            '\n' +
            "Hora: ${DateTime.now().hour}" +
            " : ${DateTime.now().minute}" +
            " : ${DateTime.now().second}" +
            "                " +
            '\n\n' +
            listP.replaceAll("{", "") +
            '\n\n' +
            "Total $total");
    _prefs.setString('keys', res + "," + keys.toString());
  }

  _cleanData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    _prefs.clear();
  }

  Future _getLenKey() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      List<String> key = _prefs.getString('keys').split(',');
      nroFac = key.length;
    });
  }

  Future _getData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      res = _prefs.getString('keys').toString();
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _getData();
    _getLenKey();
    int tot = 0;
    for (var i = 0; i < listOfColumns.length; i++) {
      tot += int.parse(listOfColumns[i]["Precio"]) *
          int.parse(listOfColumns[i]["Cantidad"]);
    }
    total = tot;
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          title: Image.asset("assets/images/LOGOTIPO.png"),
          actions: [
            Container(
                height: 50,
                width: 40,
                margin: EdgeInsets.only(top: 2, bottom: 2, right: 20),
                child: Image.asset("assets/images/LG2.png")),
          ],
        ),
        body: SingleChildScrollView(
            child: Center(
                //width: 300,
                //color: Colors.amber,
                child: Column(children: [
          SizedBox(
            height: 30,
          ),
          text(
            "Detalle Pedido de Hamburguesas",
            16.0,
            FontWeight.bold,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Factura Nro : $nroFac",
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
              "Fecha :  ${DateTime.now().day}" +
                  " - ${DateTime.now().month}" +
                  " - ${DateTime.now().year}",
              style: TextStyle(color: Colors.white)),
          Text(
              "Hora :  ${DateTime.now().hour}" +
                  " : ${DateTime.now().minute}" +
                  " : ${DateTime.now().second}",
              style: TextStyle(color: Colors.white)),
          SingleChildScrollView(
            padding: EdgeInsets.all(20),
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columnSpacing: 20,
              decoration: BoxDecoration(
                  color: Colors.blueGrey,
                  borderRadius: BorderRadius.circular(15)),
              horizontalMargin: 10,
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    'Tipo ',
                    maxLines: 1,
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.white),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Precio',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.white),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Cantidad',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.white),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Subtotal',
                    style: TextStyle(
                        fontStyle: FontStyle.italic, color: Colors.white),
                  ),
                ),
              ],
              rows:
                  listOfColumns // Loops through dataColumnText, each iteration assigning the value to element
                      .map(
                        ((element) => DataRow(
                              cells: <DataCell>[
                                DataCell(Text(
                                  element["Tipo"],
                                  maxLines: 1,
                                  style: TextStyle(color: Colors.white),
                                )), //Extracting from Map element the value
                                DataCell(Text(
                                  element["Precio"],
                                  maxLines: 1,
                                  style: TextStyle(color: Colors.white),
                                )),
                                DataCell(Text(
                                  element["Cantidad"],
                                  maxLines: 1,
                                  style: TextStyle(color: Colors.white),
                                )),
                                DataCell(Text(
                                  element["Sub"],
                                  maxLines: 1,
                                  style: TextStyle(color: Colors.white),
                                )),
                              ],
                            )),
                      )
                      .toList(),
            ),
          ),
          text("Total $tot: Bs.", 14.0, FontWeight.bold),
          SizedBox(
            height: 50,
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.all(12)),
            onPressed: () {
              if (listOfColumns.length > 0) {
                _setData();
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        contentPadding: EdgeInsets.all(10),
                        content: Text(
                          "Pedido realizado",
                          textAlign: TextAlign.center,
                        ),
                      );
                    });
                Timer(Duration(seconds: 2), () {
                  listOfColumns.clear();
                });
                Timer(Duration(seconds: 2), () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return Hamburguesas(cantidad);
                  }));
                });
              }
            },
            child: text("Confirmar Pedido", 14.0, FontWeight.bold),
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.all(12)),
            onPressed: () {
              _cleanData();
            },
            child: text("limpiar pref", 14.0, FontWeight.bold),
          ),
        ]))));
  }

  Widget text(title, size, font) {
    return Text(
      title,
      maxLines: 1,
      style: TextStyle(color: Colors.white, fontSize: size, fontWeight: font),
    );
  }
}
