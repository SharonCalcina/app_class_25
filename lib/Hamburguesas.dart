import 'package:app_class_25/Detalle.dart';
import 'package:app_class_25/HistPedidos.dart';
import 'package:flutter/material.dart';

class Hamburguesas extends StatefulWidget {
  final List<int> cantidad;
  Hamburguesas(this.cantidad);
  @override
  _HamburguesasState createState() => _HamburguesasState(this.cantidad);
}

class _HamburguesasState extends State<Hamburguesas> {
  _HamburguesasState(this.cantidad);
  List<int> cantidad;
  List<String> image = [
    "simple.jpg",
    "doble.jpg",
    "triple.jpg",
    "pollo.jpg",
    "gemelas.jpg",
    "quinoa.jpg",
    "remolacha.jpg"
  ];
  List<String> title = [
    "Hamburguesa Simple",
    "Hamburguesa Doble",
    "Hamburguesa Triple",
    "Hamburguesa de pollo",
    "Hamburguesas Gemelas",
    "Hamburguesa de Quinoa",
    "Hamburguesa de Remolacha"
  ];
  List<String> price = [
    "20",
    "35",
    "55",
    "22",
    "45",
    "20",
    "24",
  ];
  /*List<int> cantidad = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];*/

  int total = 0;

  int index = 0;
  List<Map<String, String>> listOfColumns = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          title: Image.asset("assets/images/LOGOTIPO.png"),
          actions: [
            Container(
                height: 50,
                width: 40,
                margin: EdgeInsets.only(top: 2, bottom: 2, right: 20),
                child: Image.asset("assets/images/LG2.png")),
          ],
        ),
        body: SingleChildScrollView(
          child: Center(
            //width: 300,
            //color: Colors.amber,
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Image.asset(
                    "assets/images/${image[index]}",
                    width: 250,
                    height: 250,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  title[index],
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Precio ${price[index]} Bs",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.green,
                        ),
                        onPressed: () {
                          setState(() {
                            index -= 1;
                            if (index < 0) {
                              index = image.length - 1;
                            }
                          });
                        },
                        child: Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.white,
                        )),
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.green,
                        ),
                        onPressed: () {
                          setState(() {
                            index += 1;
                            if (index > image.length - 1) {
                              index = 0;
                            }
                          });
                        },
                        child: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      button("S", 0),
                      button("D", 1),
                      button("T", 2),
                      button("P", 3),
                      button("G", 4),
                      button("Q", 5),
                      button("R", 6),
                    ]),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.grey.shade800,
                        ),
                        onPressed: () {
                          setState(() {
                            if (cantidad[index] > 0) {
                              cantidad[index] -= 1;
                              int t = 0;
                              for (var i = 0; i < cantidad.length; i++) {
                                t = t + (int.parse(price[i]) * cantidad[i]);
                              }
                              total = t;
                              for (var i = 0; i < listOfColumns.length; i++) {
                                if (listOfColumns[i].containsValue(
                                  title[index],
                                )) {
                                  if (cantidad[index] == 0) {
                                    int id = listOfColumns.indexWhere(
                                        (element) => element
                                            .containsValue(title[index]));
                                    listOfColumns.removeAt(id);
                                  } else {
                                    listOfColumns[i].update("Cantidad",
                                        (v) => cantidad[index].toString());
                                    listOfColumns[i].update(
                                        "Sub",
                                        (v) => (int.parse(price[index]) *
                                                cantidad[index])
                                            .toString());
                                  }
                                }
                              }

                              print(listOfColumns.length);
                              print(listOfColumns.toString());
                            }
                          });
                        },
                        child: Container(
                            padding: EdgeInsets.only(bottom: 5),
                            child: Icon(
                              Icons.minimize,
                              color: Colors.white,
                            ))),
                    Text(
                      cantidad[index].toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size(20, 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.grey.shade800,
                        ),
                        onPressed: () {
                          setState(() {
                            cantidad[index] += 1;
                            int t = 0;
                            for (var i = 0; i < cantidad.length; i++) {
                              t = t + (int.parse(price[i]) * cantidad[i]);
                            }
                            total = t;
                            if (listOfColumns.length == 0) {
                              listOfColumns.add({
                                "Tipo": title[index],
                                "Precio": price[index],
                                "Cantidad": cantidad[index].toString(),
                                "Sub":
                                    (int.parse(price[index]) * cantidad[index])
                                        .toString()
                              });
                            } else {
                              bool isIn = false;
                              int id = 0;
                              for (var i = 0; i < listOfColumns.length; i++) {
                                print("tipoooo");
                                print(title[index]);
                                print(listOfColumns[i]["Tipo"]);
                                print(listOfColumns[i]["Tipo"]
                                    .contains(title[index]));
                                if (listOfColumns[i]
                                    .containsValue(title[index])) {
                                  print("yessss");
                                  isIn = true;
                                  id = i;
                                }
                              }
                              if (isIn) {
                                listOfColumns[id].update("Cantidad",
                                    (v) => cantidad[index].toString());
                                listOfColumns[id].update(
                                    "Sub",
                                    (v) => (int.parse(price[index]) *
                                            cantidad[index])
                                        .toString());
                                print("trueeeeee");
                              } else {
                                listOfColumns.add({
                                  "Tipo": title[index],
                                  "Precio": price[index],
                                  "Cantidad": cantidad[index].toString(),
                                  "Sub": (int.parse(price[index]) *
                                          cantidad[index])
                                      .toString()
                                });
                              }
                            }
                            print(listOfColumns.length);
                            print(listOfColumns.toString());
                          });
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                        )),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    buttonEl("Eliminar este producto del pedido"),
                    buttonHist("Historial de  pedidos"),
                  ],
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
        ),
        bottomSheet: Container(
            padding: EdgeInsets.all(15),
            color: Colors.transparent.withOpacity(0.6),
            height: 60,
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total a pagar :   $total Bs.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
                TextButton(
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.all(5),
                        primary: Colors.black,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20)))),
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        return Detalle(listOfColumns);
                      }));
                    },
                    child: Text("Ver detalle",
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.amber.shade800,
                            fontWeight: FontWeight.bold)))
              ],
            )));
  }

  Widget button(label, i) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(12),
        minimumSize: Size(20, 20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        backgroundColor: Colors.blue.shade200.withOpacity(0.4),
      ),
      onPressed: () {
        setState(() {
          index = i;
        });
      },
      child: Text(
        label,
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),
      /*label: Text(
          label,
          style: TextStyle(color: Colors.grey.shade800),
        )*/
    );
  }

  Widget buttonEl(label) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(12.0),
        minimumSize: Size(2, 20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        primary: Colors.red.withOpacity(0.6),
      ),
      onPressed: () {
        setState(() {
          cantidad[index] = 0;
          int t = 0;
          for (var i = 0; i < cantidad.length; i++) {
            t = t + (int.parse(price[i]) * cantidad[i]);
          }
          total = t;
          int id = listOfColumns
              .indexWhere((element) => element.containsValue(title[index]));
          listOfColumns.removeAt(id);
        });
      },
      child: Text(
        label,
        style: TextStyle(
          color: Colors.white.withOpacity(0.8),
          fontSize: 12,
        ),
        maxLines: 2,
      ),
    );
  }

  Widget buttonHist(label) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(12.0),
        minimumSize: Size(2, 20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        primary: Colors.green.shade600,
      ),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return HistPedidos();
        }));
        setState(() {});
      },
      child: Text(
        label,
        style: TextStyle(
          color: Colors.white.withOpacity(0.8),
          fontSize: 12,
        ),
        maxLines: 2,
      ),
    );
  }
}
