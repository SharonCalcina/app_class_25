import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistPedidos extends StatefulWidget {
  @override
  _HistPedidosState createState() => _HistPedidosState();
}

class _HistPedidosState extends State<HistPedidos> {
  TextEditingController factura = TextEditingController();
  String _result;
  List<String> key = [];
  List pedidos = [];
  bool showId = false;
  @override
  void initState() {
    super.initState();
  }

  Future _getData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      _result = _prefs.getString('keys');
    });
  }

  Future _showData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      key = _result.split(',');
      for (var i = 0; i < key.length; i++) {
        if (_prefs.containsKey(key[i])) {
          if (!pedidos.contains(_prefs.getString(key[i]))) {
            /*String rep =
                _prefs.getString(key[i]).toString().replaceAll("{", "");
            List p = rep.split("}");
            pedidos.add(p);*/
            pedidos.add(_prefs.getString(key[i]));
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _getData();
    _showData();
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          toolbarHeight: 120,
          title: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Text("Historial de pedidos", style: TextStyle(fontSize: 16)),
              Container(
                margin: EdgeInsets.all(10),

                //padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        showId = true;
                      },
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                        padding: EdgeInsets.all(0),
                        //color: Colors.amber,
                        width: MediaQuery.of(context).size.width / 2,
                        child: TextField(
                          onChanged: (val) {
                            showId = false;
                          },
                          controller: factura,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(color: Colors.white),
                              border: InputBorder.none,
                              labelText: "Buscar por Nro de factura"),
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ))
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(30)),
              )
            ],
          ),
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.transparent,
          centerTitle: true,
        ),
        body: (showId)
            ? (int.parse(factura.value.text) <= pedidos.length)
                ? CustomScrollView(
                    slivers: [
                      SliverFixedExtentList(
                          itemExtent: 200.0,
                          delegate: SliverChildListDelegate([
                            ListTile(
                              minVerticalPadding: 10,
                              leading: Icon(
                                Icons.fastfood,
                                color: Colors.black,
                              ),
                              tileColor: Colors.amber.shade400,
                              subtitle: Text(
                                pedidos[int.parse(factura.value.text) - 1]
                                    .toString()
                                    .replaceAll("[", ""),
                                style: TextStyle(color: Colors.black),
                              ),
                              title: Text(
                                "Pedido - Factura Nro. ${int.parse(factura.value.text)}",
                                style: TextStyle(color: Colors.black),
                              ),
                            )
                          ]))
                    ],
                  )
                : SingleChildScrollView(
                    child: Center(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 50,
                        ),
                        Text(
                          "Ese numero de Factura aun NO se encuentra en tu historial",
                          maxLines: 2,
                          style: TextStyle(color: Colors.white),
                        ),
                        SizedBox(height: 50),
                        Image.asset("assets/images/compra.png")
                      ],
                    ),
                  ))
            : (pedidos.length > 0)
                ? CustomScrollView(
                    slivers: [
                      SliverFixedExtentList(
                        itemExtent: 200.0,
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int i) {
                          String pp = pedidos[i].toString().replaceAll("[", "");
                          return ListTile(
                            minVerticalPadding: 10,
                            leading: Icon(
                              Icons.fastfood,
                              color: Colors.amber.shade900,
                            ),
                            tileColor: Colors.white.withOpacity(0.8),
                            subtitle: Text(
                              pp.replaceAll("]", ""),
                              style: TextStyle(color: Colors.black),
                            ),
                            title: Text(
                              "Pedido ${i + 1}",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          );
                        }, childCount: pedidos.length),
                      )
                    ],
                  )
                : Center(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 50,
                        ),
                        Text(
                          "Aun no has hecho pedidos :(",
                          style: TextStyle(color: Colors.white),
                        ),
                        SizedBox(height: 50),
                        Image.asset("assets/images/compra.png")
                      ],
                    ),
                  ));
  }
}
