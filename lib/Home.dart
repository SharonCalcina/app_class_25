import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController bs = TextEditingController();
  final key = GlobalKey<FormState>();
  List<String> coins = ["2", "4"];
  String chile = "0";
  String dolar = "0";
  String euro = "0";
  String real = "0";
  String sol = "0";
  String yen = "0";
  String yuan = "0";
  String chi = "0.011354";
  String dol = "6.96";
  String eu = "7.52";
  String re = "2.2852";
  String so = "2.23";
  String ye = "0.05849034";
  String yu = "1.1379";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          centerTitle: true,
          title: Text(
            "Convertidor de Monedas",
            style: TextStyle(fontSize: 16),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              child: Column(
            children: [
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(10),
                child: Table(
                  columnWidths: {
                    0: FlexColumnWidth(3),
                    1: FlexColumnWidth(6),
                    2: FlexColumnWidth(2),
                  },
                  //border: TableBorder.all(width: 5, color: Colors.grey),
                  children: [
                    TableRow(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 15, bottom: 15),
                            child: Text(
                              "Bolivianos",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Form(
                              key: key,
                              child: Container(
                                  //height: 100,
                                  child: TextFormField(
                                validator: (val) => (val.isEmpty)
                                    ? "Ingrese una cifra"
                                    : (double.parse(val) > 1000)
                                        ? "Ingrese una cifra menor a 1000"
                                        : null,
                                decoration: InputDecoration(
                                  //border: InputBorder.none,
                                  hintText: "p.e. 258",
                                ),
                                keyboardType: TextInputType.number,
                                controller: bs,
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              child: Image.asset(
                                "assets/images/bs.png",
                                height: 25,
                              ))
                        ]),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              DataTable(
                  columnSpacing: MediaQuery.of(context).size.width / 32,
                  columns: [
                    DataColumn(label: Text("")),
                    DataColumn(label: Text("T.C.")),
                    DataColumn(label: Text("Equivalente")),
                    DataColumn(label: Text(""))
                  ],
                  rows: [
                    DataRow(cells: [
                      DataCell(country2("Dolares")),
                      DataCell(country(dol)),
                      DataCell(country(dolar)),
                      DataCell(
                        Image.asset(
                          "assets/images/dolar.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Euros")),
                      DataCell(country(eu)),
                      DataCell(country(euro)),
                      DataCell(
                        Image.asset(
                          "assets/images/euro.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Soles")),
                      DataCell(country(so)),
                      DataCell(country(sol)),
                      DataCell(
                        Image.asset(
                          "assets/images/sol.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Pesos Chile")),
                      DataCell(country(chi)),
                      DataCell(country(chile)),
                      DataCell(
                        Image.asset(
                          "assets/images/chile.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Reales Bdrasil")),
                      DataCell(country(re)),
                      DataCell(country(real)),
                      DataCell(
                        Image.asset(
                          "assets/images/real.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Yuan China")),
                      DataCell(country(yu)),
                      DataCell(country(yuan)),
                      DataCell(
                        Image.asset(
                          "assets/images/yuan.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Yen Japon")),
                      DataCell(country(ye)),
                      DataCell(country(yen)),
                      DataCell(
                        Image.asset(
                          "assets/images/yen.png",
                          height: 25,
                        ),
                      )
                    ]),
                  ]),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextButton(
                    onPressed: () {
                      if (key.currentState.validate()) {
                        setState(() {
                          dolar = (double.parse(bs.text.toString()) /
                                  double.parse(dol))
                              .toString();
                          print(coins);
                          euro = (double.parse(bs.text.toString()) /
                                  double.parse(eu))
                              .toString();
                          sol = (double.parse(bs.text.toString()) /
                                  double.parse(so))
                              .toString();
                          chile = (double.parse(bs.text.toString()) /
                                  double.parse(chi))
                              .toString();
                          real = (double.parse(bs.text.toString()) /
                                  double.parse(re))
                              .toString();
                          yuan = (double.parse(bs.text.toString()) /
                                  double.parse(yu))
                              .toString();
                          yen = (double.parse(bs.text.toString()) /
                                  double.parse(ye))
                              .toString();
                        });
                      }
                    },
                    child: Text(
                      "CONVERTIR",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.blue.shade800,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          dolar = "0";
                          euro = "0";
                          sol = "0";
                          chile = "0";
                          real = "0";
                          yuan = "0";
                          yen = "0";
                          bs.clear();
                        });
                      },
                      child: Text(
                        "INICIALIZAR",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: Colors.red,
                          onPrimary: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)))),
                ],
              ),
            ],
          )),
        ));
  }

  Widget country(title) {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8),
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: Colors.blue),
          textAlign: TextAlign.center,
        ));
  }

  Widget country2(title) {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8, left: 5),
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: Colors.blue),
          textAlign: TextAlign.left,
        ));
  }
}

/*Table(children: [
                TableRow(
                    /* decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.2),
                            ),*/
                    children: [
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(""),
                      ),
                      Text(
                        "T.C.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red.shade800),
                      ),
                      Text("Equivalente",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red.shade800)),
                      Text("")
                    ]),
              ]),*/
/* Table(border: TableBorder.all(), children: [
                TableRow(children: [
                  country2("Dolares"),
                  country(dol),
                  country(coins[0]),
                  Image.asset(
                    "assets/images/dolar.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Euros"),
                  country(eu),
                  country(euro),
                  Image.asset(
                    "assets/images/euro.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Soles"),
                  country(so),
                  country(sol),
                  Image.asset(
                    "assets/images/sol.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Pesos Chile"),
                  country(chi),
                  country(chile),
                  Image.asset(
                    "assets/images/chile.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Reales Brasil"),
                  country(re),
                  country(real),
                  Image.asset(
                    "assets/images/real.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Yuan China"),
                  country(yu),
                  country(yuan),
                  Image.asset(
                    "assets/images/yuan.png",
                    height: 25,
                  )
                ]),
                TableRow(children: [
                  country2("Yen Japon"),
                  country(ye),
                  country(yen),
                  Image.asset(
                    "assets/images/yen.png",
                    height: 25,
                  )
                ])
              ]),*/
