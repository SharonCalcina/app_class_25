import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Home2 extends StatefulWidget {
  @override
  _Home2State createState() => _Home2State();
}

class _Home2State extends State<Home2> {
  TextEditingController bs = TextEditingController();
  final key = GlobalKey<FormState>();
  List<String> coins = ["2", "4"];
  String boli = "0";
  String chile = "0";
  String dolar = "0";
  String euro = "0";
  String real = "0";
  String sol = "0";
  String yen = "0";
  String yuan = "0";
  String bol = "1";
  String chi = "0.011354";
  String dol = "6.96";
  String eu = "7.52";
  String re = "2.2852";
  String so = "2.23";
  String ye = "0.05849034";
  String yu = "1.1379";
  int val = 1;
  String icon = "bs.png";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          centerTitle: true,
          title: Text(
            "Convertidor de Monedas",
            style: TextStyle(fontSize: 16),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              child: Column(
            children: [
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(10),
                child: Table(
                  columnWidths: {
                    0: FlexColumnWidth(4),
                    1: FlexColumnWidth(4),
                    2: FlexColumnWidth(2),
                  },
                  //border: TableBorder.all(width: 5, color: Colors.grey),
                  children: [
                    TableRow(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        children: [
                          DropdownButton(
                            value: val,
                            items: [
                              DropdownMenuItem(
                                child: country1("Bolivianos"),
                                value: 1,
                              ),
                              DropdownMenuItem(
                                child: country1("Pesos Chile"),
                                value: 2,
                              ),
                              DropdownMenuItem(
                                  child: country1("Dolares"), value: 3),
                              DropdownMenuItem(
                                child: country1("Euros"),
                                value: 4,
                              ),
                              DropdownMenuItem(
                                child: country1("Soles"),
                                value: 5,
                              ),
                              DropdownMenuItem(
                                child: country1("Reales Brasil"),
                                value: 6,
                              ),
                              DropdownMenuItem(
                                child: country1("Yuan China"),
                                value: 7,
                              ),
                              DropdownMenuItem(
                                child: country1("Yen Japon"),
                                value: 8,
                              ),
                            ],
                            onChanged: (value) {
                              setState(() {
                                val = value;
                                if (val == 1) {
                                  icon = "bs.png";
                                }
                                if (val == 2) {
                                  icon = "chile.png";
                                }
                                if (val == 3) {
                                  icon = "dolar.png";
                                }
                                if (val == 4) {
                                  icon = "euro.png";
                                }
                                if (val == 5) {
                                  icon = "sol.png";
                                }
                                if (val == 6) {
                                  icon = "real.png";
                                }
                                if (val == 7) {
                                  icon = "yuan.png";
                                }
                                if (val == 8) {
                                  icon = "yen.png";
                                }
                              });
                            },
                          ),
                          /*Container(
                            padding: EdgeInsets.only(top: 15, bottom: 15),
                            child: Text(
                              "Bolivianos",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),*/
                          Form(
                              key: key,
                              child: Container(
                                  //height: 100,
                                  child: TextFormField(
                                validator: (val) => (val.isEmpty)
                                    ? "Ingrese una cifra"
                                    //: (double.parse(val) > 1000)
                                    //  ? "Ingrese una cifra menor a 1000"
                                    : null,
                                decoration: InputDecoration(
                                  //border: InputBorder.none,
                                  hintText: "p.e. 258",
                                ),
                                keyboardType: TextInputType.number,
                                controller: bs,
                                textAlign: TextAlign.center,
                              ))),
                          Container(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              child: Image.asset(
                                "assets/images/$icon",
                                height: 25,
                              ))
                        ]),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              DataTable(
                  columnSpacing: MediaQuery.of(context).size.width / 32,
                  columns: [
                    DataColumn(label: Text("")),
                    DataColumn(label: Text("T.C.")),
                    DataColumn(label: Text("Equivalente")),
                    DataColumn(label: Text(""))
                  ],
                  rows: [
                    DataRow(cells: [
                      DataCell(country2("Bolivianos")),
                      DataCell(country(bol)),
                      DataCell(country(boli)),
                      DataCell(
                        Image.asset(
                          "assets/images/bs.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Dolares")),
                      DataCell(country(dol)),
                      DataCell(country(dolar)),
                      DataCell(
                        Image.asset(
                          "assets/images/dolar.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Euros")),
                      DataCell(country(eu)),
                      DataCell(country(euro)),
                      DataCell(
                        Image.asset(
                          "assets/images/euro.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Soles")),
                      DataCell(country(so)),
                      DataCell(country(sol)),
                      DataCell(
                        Image.asset(
                          "assets/images/sol.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Pesos Chile")),
                      DataCell(country(chi)),
                      DataCell(country(chile)),
                      DataCell(
                        Image.asset(
                          "assets/images/chile.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Reales Bdrasil")),
                      DataCell(country(re)),
                      DataCell(country(real)),
                      DataCell(
                        Image.asset(
                          "assets/images/real.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Yuan China")),
                      DataCell(country(yu)),
                      DataCell(country(yuan)),
                      DataCell(
                        Image.asset(
                          "assets/images/yuan.png",
                          height: 25,
                        ),
                      )
                    ]),
                    DataRow(cells: [
                      DataCell(country2("Yen Japon")),
                      DataCell(country(ye)),
                      DataCell(country(yen)),
                      DataCell(
                        Image.asset(
                          "assets/images/yen.png",
                          height: 25,
                        ),
                      )
                    ]),
                  ]),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextButton(
                    onPressed: () {
                      if (key.currentState.validate()) {
                        if (val == 1) {
                          setState(() {
                            bol = "1";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "6.96";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "7.52";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "2.23";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.011354";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "2.2852";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "1.1379";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.05849034";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 2) {
                          setState(() {
                            bol = "107.549";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "724.959";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "878.438";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "199.991";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "1";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "134.983";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "112.817";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "6.90948";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 3) {
                          setState(() {
                            bol = "0.14835";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "1";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "1.21171";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "0.27587";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.00138";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "0.18619";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "0.15562";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.00953";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 4) {
                          setState(() {
                            bol = "0.12245";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "0.82538";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "1";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "0.22769";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.00114";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "0.15368";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "0.12844";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.00787";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 5) {
                          setState(() {
                            bol = "0.54258";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "3.65741";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "4.43171";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "1";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.00505";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "0.68099";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "0.56916";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.03486";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 6) {
                          setState(() {
                            bol = "0.79728";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "5.37424";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "6.51200";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "1.48257";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.00742";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "1";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "0.83633";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.05122";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 7) {
                          setState(() {
                            bol = "0.95350";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "6.42731";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "7.78802";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "1.77307";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.00888";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "1.19673";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "1";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "0.06126";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                        if (val == 8) {
                          setState(() {
                            bol = "15.57";
                            boli = (double.parse(bs.text.toString()) /
                                    double.parse(bol))
                                .toString();
                            dol = "104.94";
                            dolar = (double.parse(bs.text.toString()) /
                                    double.parse(dol))
                                .toString();
                            eu = "127.14";
                            euro = (double.parse(bs.text.toString()) /
                                    double.parse(eu))
                                .toString();
                            so = "28.95";
                            sol = (double.parse(bs.text.toString()) /
                                    double.parse(so))
                                .toString();
                            chi = "0.14";
                            chile = (double.parse(bs.text.toString()) /
                                    double.parse(chi))
                                .toString();
                            re = "19.54";
                            real = (double.parse(bs.text.toString()) /
                                    double.parse(re))
                                .toString();
                            yu = "16.33";
                            yuan = (double.parse(bs.text.toString()) /
                                    double.parse(yu))
                                .toString();
                            ye = "1";
                            yen = (double.parse(bs.text.toString()) /
                                    double.parse(ye))
                                .toString();
                          });
                        }
                      }
                    },
                    child: Text(
                      "CONVERTIR",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.blue.shade800,
                        onSurface: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          boli = "0";

                          dolar = "0";
                          euro = "0";
                          sol = "0";
                          chile = "0";
                          real = "0";
                          yuan = "0";
                          yen = "0";
                          bs.clear();
                        });
                      },
                      child: Text(
                        "INICIALIZAR",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                          onPrimary: Colors.red,
                          onSurface: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)))),
                ],
              ),
            ],
          )),
        ));
  }

  Widget country(title) {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8),
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: Colors.blue),
          textAlign: TextAlign.center,
        ));
  }

  Widget country2(title) {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8, left: 5),
        child: Text(
          title,
          style: TextStyle(fontSize: 14, color: Colors.blue),
          textAlign: TextAlign.left,
        ));
  }

  Widget country1(title) {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 8, left: 5),
        child: Text(
          title,
          style: TextStyle(
              fontSize: 14,
              color: Colors.green.shade800,
              fontWeight: FontWeight.bold),
          textAlign: TextAlign.left,
        ));
  }
}

/*Table(children: [
                TableRow(
                    /* decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.2),
                            ),*/
                    children: [
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(""),
                      ),
                      Text(
                        "T.C.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red.shade800),
                      ),
                      Text("Equivalente",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red.shade800)),
                      Text("")
                    ]),
              ]),*/
/* Table(border: TableBorder.all(), children: [
                TableRow(children: [
                  country2("Dolares"),
                  country(dol),
                  country(coins[0]),
                  Image.asset(
                    "assets/images/dolar.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Euros"),
                  country(eu),
                  country(euro),
                  Image.asset(
                    "assets/images/euro.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Soles"),
                  country(so),
                  country(sol),
                  Image.asset(
                    "assets/images/sol.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Pesos Chile"),
                  country(chi),
                  country(chile),
                  Image.asset(
                    "assets/images/chile.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Reales Brasil"),
                  country(re),
                  country(real),
                  Image.asset(
                    "assets/images/real.png",
                    height: 25,
                  ),
                ]),
                TableRow(children: [
                  country2("Yuan China"),
                  country(yu),
                  country(yuan),
                  Image.asset(
                    "assets/images/yuan.png",
                    height: 25,
                  )
                ]),
                TableRow(children: [
                  country2("Yen Japon"),
                  country(ye),
                  country(yen),
                  Image.asset(
                    "assets/images/yen.png",
                    height: 25,
                  )
                ])
              ]),*/
