import 'package:app_class_25/Alimentos.dart';
import 'package:app_class_25/Calculadora.dart';
import 'package:app_class_25/Home.dart';
import 'package:app_class_25/Home2.dart';
import 'package:app_class_25/MainHam.dart';
import 'package:app_class_25/Totalizadores/mainTot.dart';
import 'package:app_class_25/consultaProd/mainCP.dart';
import 'package:app_class_25/files/menuCoop.dart';
import 'package:app_class_25/helados/menuHel.dart';
import 'package:app_class_25/planificacion/mainPlan.dart';
import 'package:flutter/material.dart';
import 'dptos.dart';

class Inicio extends StatefulWidget {
  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("INF-325 APPS"),
      ),
      body: SingleChildScrollView(
          child: Center(
        child: Column(
          children: [
            button("Conversor de Monedas", Home2()),
            button("Conversor de Monedas BS", Home()),
            button("App Hamburguesas", MainHam()),
            button("Postres", Alimentos()),
            button("Calculadora", Calculadora()),
            button("App Helados", MenuHel()),
            button("Dptos", ReadWriteFile()),
            button("External Storage", MenuCoop()),
            button("External Storage", MainTot()),
            button("Planifica", MainPlan()),
            button("Consulta Productos", MainCP())
          ],
        ),
      )),
    );
  }

  Widget button(title, page) {
    return TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        ),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return page;
          }));
        },
        child: Text(title));
  }
}
