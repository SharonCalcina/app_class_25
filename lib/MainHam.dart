import 'package:app_class_25/Hamburguesas.dart';
import 'package:flutter/material.dart';

class MainHam extends StatelessWidget {
  final List<int> cantidad = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Opacity(
        opacity: 0.6,
        child: Image.asset(
          "assets/images/ham2.jpg",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 80, left: 25, right: 25),
          child: Column(
            children: [
              Image.asset("assets/images/LOGOTIPO.png"),
              SizedBox(
                height: MediaQuery.of(context).size.height / 2.5,
              ),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Hamburguesas(cantidad);
                    }));
                  },
                  style: TextButton.styleFrom(
                      padding: EdgeInsets.all(15),
                      backgroundColor: Colors.white.withOpacity(0.5),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ))),
                  child: Text(
                    "Ir a Menu",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ))
    ]);
  }
}
