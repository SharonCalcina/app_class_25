import 'package:app_class_25/Totalizadores/profTot.dart';
import 'package:app_class_25/Totalizadores/total.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainTot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Opacity(
              opacity: 0.5,
              child: Image.asset(
                "assets/images/cop.jpg",
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              )),
          Center(
            child: Column(
              children: [
                SizedBox(
                  height: 150,
                ),
                Image.asset(
                  "assets/images/CC.png",
                  width: MediaQuery.of(context).size.width / 1.2,
                ),
                Text(
                  "TOTALIZADORES",
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2.5,
                ),
                button("Por Departamento", Totalizador(), context),
                SizedBox(
                  height: 10,
                ),
                button("Por Profesión", ProfTot(), context),
                SizedBox(
                  height: 10,
                ),
                button2("Finalizar"),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          )
        ],
      )),
    );
  }

  Widget button(title, page, context) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.cyan.shade800,
          padding: EdgeInsets.all(12)),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return page;
        }));
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }

  Widget button2(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.cyan.shade800,
          padding: EdgeInsets.all(12)),
      onPressed: () {
        SystemNavigator.pop();
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
