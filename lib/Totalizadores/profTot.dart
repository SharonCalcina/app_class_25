import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:ext_storage/ext_storage.dart';
import 'package:permission_handler/permission_handler.dart';

class ProfTot extends StatefulWidget {
  @override
  _ProfTotState createState() => _ProfTotState();
}

class _ProfTotState extends State<ProfTot> {
  Stream<int> timerStream;
  StreamSubscription<int> timerSubscription;
  String hoursStr = "00";
  String minutesStr = "00";
  String secondsStr = "00";
  bool flag = true;

  Stream<int> stopWatchStream() {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }

  int nroC = 0;
  String readRes = "";
  StringBuffer contents = StringBuffer();
  StringBuffer contents2 = StringBuffer();
  StringBuffer contents3 = StringBuffer();
  StringBuffer contents4 = StringBuffer();
  StringBuffer contents5 = StringBuffer();
  String readRes2 = "";
  String readRes3 = "";
  String readRes4 = "";
  String readRes5 = "";
  String nombre = "Sin Resultado";
  String cuentas = "Sin Saldo";
  String profesion = "Sin resultado";
  String prof = "Sin resultado";
  bool _val = false;
  List<String> listCard = [];
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/clientes.txt');
  }

  Future get _localFile2 async {
    final path = await _getFile();
    print(path);
    return File('$path/cuentas.txt');
  }

  Future get _localFile3 async {
    final path = await _getFile();
    print(path);
    return File('$path/movimientos.txt');
  }

  Future get _localFile4 async {
    final path = await _getFile();
    print(path);
    return File('$path/profesiones.txt');
  }

  Future get _localFile5 async {
    final path = await _getFile();
    print(path);
    return File('$path/deptos.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        final file2 = await _localFile2;
        final file3 = await _localFile3;
        final file4 = await _localFile4;
        final file5 = await _localFile5;
        print("--------------rrrrrrrrrrrrrr------------");
        setState(() async {
          //readRes = await file.readAsString();
          var contentStream = file.openRead();

          contentStream.transform(Utf8Decoder()).transform(LineSplitter()).listen(
              (String line) => contents
                  .write(line + "\n"), // Add line to our StringBuffer object
              onDone: () => readRes = contents
                  .toString(), // Call toString() method to receive the complete data
              onError: (e) => print('[Problems]: $e'));
          //readRes2 = await file2.readAsString();
          var contentStream2 = file2.openRead();

          contentStream2
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents2.write(line + "\n"),
                  onDone: () => readRes2 = contents2.toString(),
                  onError: (e) => print('[Problems]: $e'));

          //readRes3 = await file3.readAsString();
          var contentStream3 = file3.openRead();
          contentStream3
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents3.write(line + "\n"),
                  onDone: () => readRes3 = contents3.toString(),
                  onError: (e) => print('[Problems]: $e'));

          //readRes4 = await file4.readAsString();
          var contentStream4 = file4.openRead();
          contentStream4
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents4.write(line + "\n"),
                  onDone: () => readRes4 = contents4.toString(),
                  onError: (e) => print('[Problems]: $e'));

          //readRes5 = await file5.readAsString();
          var contentStream5 = file5.openRead();
          contentStream5
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents5.write(line + "\n"),
                  onDone: () => readRes5 = contents5.toString(),
                  onError: (e) => print('[Problems]: $e'));
        });
        return await file2.openRead();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  _busqueda(String codProf) {
    List<String> data22 = readRes4.split("\n");
    //print(data22.toString());
    print(data22.length);

    for (var i = 1; i < data22.length - 1; i++) {
      List<String> data2 = data22[i].split(";");
      //print(data2.toString());
      if (data2[0].contains(codProf)) {
        setState(() {
          prof = data2[1];
        });
        break;
      } else {
        setState(() {
          prof = "No se ha encontrado coincidencia";
        });
      }
    }
    List<String> cis = [];
    List<String> data = readRes.split("\n");
    print("llllllllllllliiiist");
    print(data.toString());
    print("llllllllllllennnn");
    print(data.length);

    for (var i = 1; i < data.length - 1; i++) {
      List<String> data2 = data[i].split(";");
      //print(data2.toString());
      //print(data2[2].toString());
      if (data2[2].toString() == codProf) {
        setState(() {
          cis.add(data2[0].substring(0, 7));
        });
      }
    }
    print("------------------------------");
    //print(cis.toString());
    print(cis.length);
    setState(() {
      listCard = cis;
    });
  }

  _busqueda3(List cis) {
    List<String> data = readRes2.split("\n");
    print("llllllllllllliiiist");
    print(data[1].toString());
    print("llllllllllllennnn");
    print(data.length);
    int nro = 0;
    List<String> cuentas1 = [];
    for (var i = 0; i < cis.length; i++) {
      String ci = cis[i];
      for (var i = 1; i < data.length - 1; i++) {
        List<String> data2 = data[i].split(";");
        // print(data2[1].substring(0, 7).toString());
        if (data2[1].substring(0, 7) == ci) {
          print("*********************");
          setState(() {
            cuentas1.add(data2[0].toString());
            nro += 1;
          });
        }
      }
    }
    print(cuentas1.toString());
    print(cuentas1.length);

    nroC = nro;
    int monto = 0;
    List<String> depositos = readRes3.split("\n");
    print("llllllllllllliiiist");
    print(depositos.length);
    for (var k = 0; k < cuentas1.length; k++) {
      for (var i = 1; i < depositos.length - 1; i++) {
        List<String> depo2 = depositos[i].split(";");
        //print(depo2[0].toString());
        if (depo2[0] == cuentas1[k]) {
          monto += int.parse(depo2[1]);
        }
      }
    }
    print(monto);
    setState(() {
      cuentas = monto.toString();
    });

    timerSubscription.cancel();
  }

  @override
  void initState() {
    super.initState();
    _readFile();
  }

  TextEditingController departamento = new TextEditingController();
  String val = "";
  final key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        actions: [
          Container(
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                "assets/images/coop.png",
                height: 20,
                width: 50,
              ))
        ],
        centerTitle: true,
        title: Image.asset(
          "assets/images/CC.png",
          width: 200,
        ),
        backgroundColor: Colors.transparent,
        elevation: 500,
        shadowColor: Colors.white,
      ),
      body: SingleChildScrollView(
          child: Center(
              child: Stack(
        children: [
          Column(
            children: [
              SizedBox(
                height: 60,
              ),
              Image.asset(
                "assets/images/prof.png",
                height: 200,
                //color: Colors.amber,
              ),
              Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  margin:
                      EdgeInsets.only(top: 20, bottom: 20, left: 50, right: 50),
                  child: Form(
                      key: key,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (text) {
                          if (text == null || text.isEmpty) {
                            return 'Error. Ingrese un Código de Profesión';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white),
                        controller: departamento,
                        onChanged: (value) {
                          val = value;
                        },
                        decoration: InputDecoration(
                          errorStyle: TextStyle(
                              fontSize: 12, fontStyle: FontStyle.italic),
                          labelText: "Ingrese el Codigo de Profesión",
                          labelStyle: TextStyle(
                              fontSize: 13.5,
                              color: Colors.white.withOpacity(0.6)),
                        ),
                      ))),
              TextButton(
                onPressed: () {
                  timerStream = stopWatchStream();
                  timerSubscription = timerStream.listen((int newTick) {
                    setState(() {
                      hoursStr = ((newTick / (60 * 60)) % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      minutesStr = ((newTick / 60) % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      secondsStr =
                          (newTick % 60).floor().toString().padLeft(2, '0');
                    });
                  });
                  _busqueda(val);
                  _busqueda3(listCard);
                },
                child: Text(
                  "Procesar",
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colors.cyan,
                    padding: EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                "Profesión:   $prof",
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "Total Depositos:   $cuentas",
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
              SizedBox(
                height: 25,
              ),
              TextButton(
                onPressed: () {
                  setState(() {
                    cuentas = "Ninguno";
                    prof = "Ninguno";
                    departamento.clear();
                  });
                },
                child: Text(
                  "Limpiar",
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
                style: TextButton.styleFrom(
                    backgroundColor: Colors.red.shade800,
                    padding: EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                "$hoursStr:$minutesStr:$secondsStr",
                style: TextStyle(fontSize: 35.0, color: Colors.white),
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
          Visibility(
            child: Container(
              color: Colors.white.withOpacity(0.2),
              padding:
                  EdgeInsets.only(top: 250, bottom: 350, left: 100, right: 100),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: CircularProgressIndicator(
                strokeWidth: 10,
                backgroundColor: Colors.amber,
              ),
            ),
            visible: _val,
          ),
        ],
      ))),
    );
  }
}
