import 'dart:async';
import 'dart:io';

import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import 'DataBaseHelper.dart';

class HomeCP extends StatefulWidget {
  @override
  _HomeCPState createState() => _HomeCPState();
}

class _HomeCPState extends State<HomeCP> {
  final dbHelper = DatabaseHelper.instance;
  String show = "";
  String tant = "";
  String tact = "";
  List<String> datos = [];
  TextEditingController codigo = new TextEditingController();
  String val = "";
  final key = GlobalKey<FormState>();
  String readRes = "";
  String readRes2 = "";
  String readRes3 = "";
  String des = "";
  String univen = "";
  String lines = "";
  String precio = "";
  String costo = "";
  String exis = "";
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/lineas.txt');
  }

  Future get _localFile2 async {
    final path = await _getFile();
    print(path);
    return File('$path/unidades.txt');
  }

  Future get _localFile3 async {
    final path = await _getFile();
    print(path);
    return File('$path/productos.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        final file2 = await _localFile2;
        final file3 = await _localFile3;
        print("--------------rrrrrrrrrrrrrr------------");
        setState(() async {
          readRes = await file.readAsString();
          readRes2 = await file2.readAsString();
          readRes3 = await file3.readAsString();
        });
        return await file3.readAsString();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  List<String> res = [];

  _busqueda() {
    List<String> data = readRes.split("\n");
    print("llllllllllllliiiist");
    print(data.toString());
    print("llllllllllllennnn");
    print(data.length);
    setState(() {
      res = data;
    });
  }

  List<String> res2 = [];
  _busqueda2() {
    List<String> data = readRes2.split("\n");
    print(data.toString());
    print(data.length);
    setState(() {
      res2 = data;
    });
  }

  List<String> res3 = [];
  _busqueda3() {
    List<String> data = readRes3.split("\n");
    print(data.toString());
    print(data.length);
    setState(() {
      res3 = data;
    });
  }

  List<String> res4 = [];
  String unidad = "";
  String linea = "";
  _busqueda4(String code) {
    String lin = "";
    String uni = "";
    List<String> data = readRes3.split("\n");
    bool close = false;
    for (var i = 1; i < data.length; i++) {
      List<String> data2 = data[i].split(";");
      if (data2[0] == code) {
        setState(() {
          res4 = data2;
          lin = data2[2];
          uni = data2[4];
          des = data2[1];
          precio = data2[5];
          costo = data2[6];
          exis = data2[7];
        });
        break;
      } else {
        setState(() {
          close = true;
          des = "";
          lin = "";
          uni = "";
          precio = "";
          costo = "";
          exis = "";
        });
      }
    }
    if (close) {
      timerSubscription.cancel();
      hoursStr = "00";
      minutesStr = "00";
      secondsStr = "00";
    } else {
      List<String> data11 = readRes2.split("\n");
      for (var i = 1; i < data11.length; i++) {
        List<String> data2 = data11[i].split(";");
        if (data2[0] == lin) {
          setState(() {
            univen = data2[1].toString();
          });
          break;
        } else {
          setState(() {
            univen = "";
          });
        }
      }
      List<String> data22 = readRes.split("\n");
      print(data22[1].toString());
      print(data22.length);
      for (var i = 1; i < data22.length; i++) {
        List<String> data2 = data22[i].split(";");
        if (data2[0] == uni) {
          setState(() {
            // linea = data2[1];
            lines = data2[1];
          });
          break;
        } else {
          setState(() {
            lines = "";
          });
        }
      }
    }
  }

  Stream<int> timerStream;
  StreamSubscription<int> timerSubscription;
  String hoursStr = "00";
  String minutesStr = "00";
  String secondsStr = "00";
  bool flag = true;
  Stream<int> stopWatchStream() {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }

  @override
  void initState() {
    _readFile();
    super.initState();
    //timerSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          actions: [
            Container(
              margin: EdgeInsets.only(right: 20),
              child: Image.asset("assets/images/mp.png"),
            )
          ],
          backgroundColor: Colors.transparent,
          elevation: 100,
          shadowColor: Colors.amber,
          title: Image.asset("assets/images/log.png"),
        ),
        body: SingleChildScrollView(
            child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /*ElevatedButton(
              child: Text('insert', style: TextStyle(fontSize: 20),),
              onPressed: _insert,
            ),*/
              Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  margin:
                      EdgeInsets.only(top: 20, bottom: 20, left: 50, right: 50),
                  child: Form(
                      key: key,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (text) {
                          if (text == null || text.isEmpty) {
                            return 'Error. Ingrese un codigo de Producto';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white),
                        controller: codigo,
                        onChanged: (value) {
                          val = value;
                        },
                        decoration: InputDecoration(
                          errorStyle: TextStyle(
                              fontSize: 12, fontStyle: FontStyle.italic),
                          labelText: "Ingrese el Codigo de Producto",
                          labelStyle: TextStyle(
                              fontSize: 13.5,
                              color: Colors.white.withOpacity(0.6)),
                        ),
                      ))),
              /*Text(
                res4.toString(),
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),*/
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.green,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20)))),
                    child: Text(
                      'BUSCA BD',
                      style: TextStyle(fontSize: 15),
                    ),
                    onPressed: () async {
                      if (mounted) {
                        if (key.currentState.validate()) {
                          timerStream = stopWatchStream();
                          timerSubscription = timerStream.listen((int newTick) {
                            setState(() {
                              hoursStr = ((newTick / (60 * 60)) % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                              minutesStr = ((newTick / 60) % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                              secondsStr = (newTick % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                            });
                          });
                          _query(codigo.value.text);
                          setState(() {
                            tant =
                                hoursStr + ":" + minutesStr + ":" + secondsStr;
                            timerSubscription.cancel();
                            hoursStr = "00";
                            minutesStr = "00";
                            secondsStr = "00";
                          });
                        }
                      }
                    },
                  ),
                  ElevatedButton(
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.green.shade800,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20)))),
                    child: Text(
                      'BUSCA TXT',
                      style: TextStyle(fontSize: 15),
                    ),
                    onPressed: () async {
                      if (mounted) {
                        if (key.currentState.validate()) {
                          timerStream = stopWatchStream();
                          timerSubscription = timerStream.listen((int newTick) {
                            setState(() {
                              hoursStr = ((newTick / (60 * 60)) % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                              minutesStr = ((newTick / 60) % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                              secondsStr = (newTick % 60)
                                  .floor()
                                  .toString()
                                  .padLeft(2, '0');
                            });
                          });
                          _busqueda4(codigo.value.text);
                          setState(() {
                            tant =
                                hoursStr + ":" + minutesStr + ":" + secondsStr;
                            timerSubscription.cancel();
                            hoursStr = "00";
                            minutesStr = "00";
                            secondsStr = "00";
                          });
                        }
                      }
                    },
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1,
                margin:
                    EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white.withOpacity(0.3),
                ),
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    text("DESCRIPCION: $des"),
                    text("UNIVEN: $univen"),
                    text("LINEA: $lines"),
                    text("PRECIO VENTA: $precio"),
                    text("COSTO PROD: $costo"),
                    text("EXISTENCIA: $exis"),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1,
                margin:
                    EdgeInsets.only(top: 0, bottom: 10, left: 20, right: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white.withOpacity(0.3)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Align(
                      child: text2("Tiempo Anterior: $tant"),
                      alignment: Alignment.center,
                    ),
                    text2("Tiempo Actual: $hoursStr:$minutesStr:$secondsStr")
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                style: TextButton.styleFrom(
                    backgroundColor: Colors.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)))),
                child: Text(
                  'LIMPIAR',
                  style: TextStyle(fontSize: 15),
                ),
                onPressed: () {
                  setState(() {
                    codigo.clear();
                    des = "";
                    univen = "";
                    lines = "";
                    precio = "";
                    costo = "";
                    exis = "";
                    tant = "";
                  });
                },
              ),
            ],
          ),
        )));
  }

  Widget text(title) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        title,
        style: TextStyle(fontSize: 15, color: Colors.white),
      ),
    );
  }

  Widget text2(title) {
    return Text(
      title,
      style: TextStyle(fontSize: 15, color: Colors.white),
    );
  }

  void _insert() async {
    for (var i = 1; i < res.length - 1; i++) {
      List<String> data2 = res[i].split(";");
      Map<String, dynamic> row = {
        DatabaseHelper.columnIdl: int.parse(data2[0]),
        DatabaseHelper.columnDesL: data2[1]
      };
      final id = await dbHelper.insert(row);
      print('inserted row id: $id');
    }
  }

  void _insert2() async {
    for (var i = 1; i < res2.length - 1; i++) {
      List<String> data2 = res2[i].split(";");
      Map<String, dynamic> row = {
        DatabaseHelper.columnIdu: data2[0],
        DatabaseHelper.columnDesU: data2[1]
      };
      final id = await dbHelper.insert2(row);
      print('inserted row id: $id');
    }
  }

  void _insert3() async {
    for (var i = 1; i < res3.length - 1; i++) {
      List<String> data2 = res3[i].split(";");
      Map<String, dynamic> row = {
        DatabaseHelper.columnCodprod: int.parse(data2[0]),
        DatabaseHelper.columnDesP: data2[1],
        DatabaseHelper.columnUniven: data2[2],
        DatabaseHelper.columnUniem: int.parse(data2[3]),
        DatabaseHelper.columnLinea: int.parse(data2[4]),
        DatabaseHelper.columnPreciov: double.parse(data2[5]),
        DatabaseHelper.columnCostop: double.parse(data2[6]),
        DatabaseHelper.columnExis: int.parse(data2[7])
      };
      final id = await dbHelper.insert3(row);
      print('inserted row id: $id');
    }
  }

  Future _query(String code) async {
    final allRows = await dbHelper.queryCodProd(code);
    if (!allRows.isEmpty) {
      print('query all rows:');
      allRows.forEach(print);
      String gotdata = allRows.toString();
      setState(() {
        show = gotdata;
        datos = gotdata.split(",");
        //datos[0].split(":")[1].toString();
        des = datos[1].split(":")[1].toString();
        univen = datos[2].split(":")[1].toString();
        lines = datos[3].split(":")[1].toString();
        precio = datos[5].split(":")[1].toString();
        costo = datos[6].split(":")[1].toString();
        exis = datos[7].split(":")[1].split("}")[0].toString();
      });
    }
  }

  /* void _update() async {
    // row to update
    Map<String, dynamic> row = {
      DatabaseHelper.columnId   : 1,
      DatabaseHelper.columnName : 'Mary',
      DatabaseHelper.columnAge  : 32
    };
    final rowsAffected = await dbHelper.update(row);
    print('updated $rowsAffected row(s)');
  }

  void _delete() async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(id);
    print('deleted $rowsDeleted row(s): row $id');
  }*/
}
