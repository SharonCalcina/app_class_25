import 'package:app_class_25/consultaProd/homeCP.dart';
import 'package:flutter/material.dart';

class MainCP extends StatefulWidget {
  @override
  _MainCPState createState() => _MainCPState();
}

class _MainCPState extends State<MainCP> {
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Opacity(
        opacity: 0.6,
        child: Image.asset(
          "assets/images/prod2.jpg",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: 120, left: 25, right: 25),
          child: Column(
            children: [
              Image.asset("assets/images/log.png"),
              SizedBox(
                height: MediaQuery.of(context).size.height / 2.5,
              ),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return HomeCP();
                    }));
                  },
                  style: TextButton.styleFrom(
                      padding: EdgeInsets.all(15),
                      backgroundColor: Colors.white.withOpacity(0.6),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ))),
                  child: Text(
                    "Ir a Menu",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ))
    ]);
  }
}
