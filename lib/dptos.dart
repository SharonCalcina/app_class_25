import 'dart:async';
import 'dart:io';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class _ReadWriteFileAppState extends State<ReadWriteFile> {
  String readRes = "";

  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/platos.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        print("--------------rrrrrrrrrrrrrr------------");
        print(file.readAsString());

        setState(() async {
          readRes = await file.readAsString();
        });
        return await file.readAsString();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  @override
  void initState() {
    super.initState();
    _readFile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(readRes),
        ),
        body: Center(child: Text(readRes)));
  }
}

class ReadWriteFile extends StatefulWidget {
  @override
  _ReadWriteFileAppState createState() => new _ReadWriteFileAppState();
}

/*Future get _localPath async {

    final externalDirectory = await getExternalStorageDirectory();
    return externalDirectory.path;
  }*/

// Application documents directory: /data/user/0/{package_name}/{app_name}
//final applicationDirectory = await getApplicationDocumentsDirectory();
// External storage directory: /storage/emulated/0
// Application temporary directory: /data/user/0/{package_name}/cache
//final tempDirectory = await getTemporaryDirectory();
//bool _allowWriteFile = false;
//   //this._writeToFile("Test");

/*Future _writeToFile(String text) async {
    if (!_allowWriteFile) {
      return null;
    }

    final file = await _localFile;

    // Write the file
    File result = await file.writeAsString('$text');

    if (result == null) {
      print("Writing to file failed");
    } else {
      print("Successfully writing to file");

      print("Reading the content of file");
      String readResult = await _readFile();
      print("readResult: " + readResult.toString());
    }
  }*/
