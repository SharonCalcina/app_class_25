import 'dart:io';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class Cooperativas extends StatefulWidget {
  @override
  _CooperativasState createState() => _CooperativasState();
}

class _CooperativasState extends State<Cooperativas> {
  int nroC = 0;
  String readRes = "";
  String readRes2 = "";
  String readRes3 = "";
  String readRes4 = "";
  String readRes5 = "";
  String nombre = "Sin Resultado";
  String cuentas = "Sin Saldo";
  String profesion = "Sin resultado";
  String dpto = "Sin resultado";
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/clientes.txt');
  }

  Future get _localFile2 async {
    final path = await _getFile();
    print(path);
    return File('$path/cuentas.txt');
  }

  Future get _localFile3 async {
    final path = await _getFile();
    print(path);
    return File('$path/movimientos.txt');
  }

  Future get _localFile4 async {
    final path = await _getFile();
    print(path);
    return File('$path/profesiones.txt');
  }

  Future get _localFile5 async {
    final path = await _getFile();
    print(path);
    return File('$path/deptos.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        final file2 = await _localFile2;
        final file3 = await _localFile3;
        final file4 = await _localFile4;
        final file5 = await _localFile5;
        print("--------------rrrrrrrrrrrrrr------------");
        setState(() async {
          readRes = await file.readAsString();
          readRes2 = await file2.readAsString();
          readRes3 = await file3.readAsString();
          readRes4 = await file4.readAsString();
          readRes5 = await file5.readAsString();
        });
        return await file2.readAsString();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  _busqueda(String ci) {
    String codDep = "";
    String codProf = "";
    List<String> data = readRes.split("\n");
    print("llllllllllllliiiist");
    print(data[1].toString());
    print("llllllllllllennnn");
    print(data.length);

    for (var i = 1; i < data.length; i++) {
      List<String> data2 = data[i].split(";");
      print(data2.toString());
      if (data2[0] == ci) {
        setState(() {
          nombre = data2[1];
          codProf = data2[2];
          codDep = data2[3].substring(0, 1);
          print("///////////////");
          print(codDep);
        });
        break;
      } else {
        nombre =
            "No se ha encontrado ningun usuario con ese CI. Intente de nuevo";
      }
    }

    List<String> data11 = readRes4.split("\n");
    print(data11[1].toString());
    print(data11.length);

    for (var i = 1; i < data11.length; i++) {
      List<String> data2 = data11[i].split(";");
      print(data2.toString());
      if (data2[0] == codProf) {
        setState(() {
          profesion = data2[1];
          dpto = codDep;
        });
        break;
      }
    }

    List<String> data22 = readRes5.split("\n");
    print(data22[1].toString());
    print(data22.length);

    for (var i = 1; i < data22.length; i++) {
      List<String> data2 = data22[i].split(";");
      print(data2.toString());
      if (data2[0].contains(codDep)) {
        setState(() {
          dpto = data2[1];
        });
        break;
      }
    }
  }

  _busqueda3(String ci) {
    List<String> data = readRes2.split("\n");
    print("llllllllllllliiiist");
    print(data[1].toString());
    print("llllllllllllennnn");
    print(data.length);
    int nro = 0;
    String c = "";
    List<String> cuentas1 = [];
    for (var i = 1; i < data.length - 1; i++) {
      List<String> data2 = data[i].split(";");
      print(data2[1].substring(0, 7).toString());
      if (data2[1].substring(0, 7) == ci) {
        print("*********************");
        setState(() {
          cuentas1.add(data2[0].toString());
          c = data2[0].toString();
          nro += 1;
        });
      }
    }
    nroC = nro;
    print(c);
    int monto = 0;
    List<String> depositos = readRes3.split("\n");
    print("llllllllllllliiiist");
    print(depositos.toString());
    for (var k = 0; k < cuentas1.length; k++) {
      for (var i = 1; i < depositos.length; i++) {
        List<String> depo2 = depositos[i].split(";");
        print(depo2[0].toString());
        if (depo2[0] == cuentas1[k]) {
          monto += int.parse(depo2[1]);
        }
      }
    }
    print(monto);
    setState(() {
      cuentas = monto.toString();
    });
    /*for (var i = 1; i < depositos.length; i++) {
      List<String> depo2 = depositos[i].split(";");
      print(depo2[0].toString());
      if (depo2[0] == c) {
        monto += int.parse(depo2[1]);
      }
    }
    print(monto);
    setState(() {
      cuentas = monto.toString();
    });*/
  }

  @override
  void initState() {
    super.initState();
    _readFile();
  }

  TextEditingController carnet = new TextEditingController();
  String val = "";
  final key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        actions: [
          Container(
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                "assets/images/coop.png",
                height: 20,
                width: 50,
              ))
        ],
        centerTitle: true,
        title: Image.asset(
          "assets/images/CC.png",
          width: 200,
        ),
        backgroundColor: Colors.transparent,
        elevation: 500,
        shadowColor: Colors.white,
      ),
      body: SingleChildScrollView(
          child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            Image.asset(
              "assets/images/ico.png",
              height: 100,
              color: Colors.amber,
            ),
            Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                margin:
                    EdgeInsets.only(top: 20, bottom: 20, left: 50, right: 50),
                child: Form(
                    key: key,
                    child: TextFormField(
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'Error. Ingrese su CI';
                        }
                        return null;
                      },
                      style: TextStyle(color: Colors.white),
                      controller: carnet,
                      onChanged: (value) {
                        val = value;
                      },
                      decoration: InputDecoration(
                        errorStyle: TextStyle(
                            fontSize: 12, fontStyle: FontStyle.italic),
                        labelText: "Ingrese su Carnet de Identidad",
                        labelStyle: TextStyle(
                            fontSize: 13.5,
                            color: Colors.white.withOpacity(0.6)),
                      ),
                    ))),
            TextButton(
              onPressed: () {
                if (key.currentState.validate()) {
                  _busqueda(val);
                  _busqueda3(val);
                }
              },
              child: Text(
                "Buscar",
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.cyan,
                  padding: EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              "Nombres:   $nombre",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Profesion:   $profesion",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Departamento:   $dpto",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "nro de cuentas:   $nroC",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "Saldo Total:   $cuentas",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
            SizedBox(
              height: 25,
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  nombre = "Ninguno";
                  cuentas = "Ninguno";
                  profesion = "Ninguno";
                  dpto = "Ninguno";
                  nroC = 0;
                  carnet.clear();
                });
              },
              child: Text(
                "Limpiar",
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red.shade800,
                  padding: EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            ),
            Text(readRes2)
          ],
        ),
      )),
    );
  }
}
