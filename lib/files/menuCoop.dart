import 'package:app_class_25/files/cooperativas.dart';
import 'package:flutter/material.dart';

class MenuCoop extends StatefulWidget {
  @override
  _MenuCoopState createState() => _MenuCoopState();
}

class _MenuCoopState extends State<MenuCoop> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Opacity(
              opacity: 0.5,
              child: Image.asset(
                "assets/images/cop.jpg",
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              )),
          Center(
            child: Column(
              children: [
                SizedBox(
                  height: 150,
                ),
                Image.asset(
                  "assets/images/CC.png",
                  width: MediaQuery.of(context).size.width / 1.2,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                ),
                /*Image.asset(
                  "assets/images/im0.png",
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),*/
                button("Consultar Saldo por CI", Cooperativas()),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          )
        ],
      )),
    );
  }

  Widget button(title, page) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.cyan.shade800,
          padding: EdgeInsets.all(12)),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return page;
        }));
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
