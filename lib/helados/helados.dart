import 'package:flutter/material.dart';
//import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
//import 'dart:convert';
//import 'dart:io';
//import 'package:path_provider/path_provider.dart';

class Helados extends StatefulWidget {
  final int numH;
  Helados(this.numH);
  @override
  _HeladosState createState() => _HeladosState(this.numH);
}

class _HeladosState extends State<Helados> {
  _HeladosState(this.numH);
  List<String> image = ["im1", "im2", "im3", "im4"];
  int numH;
  List data = [];
  List data2 = [];
  String _content = "";
  /*Future loadAsset(BuildContext context) async {
    final _loadedData =
        await rootBundle.loadString('assets/images/recetas.txt');

    setState(() {
      _data = _loadedData;
    });
  }*/

  Future<void> _loadAsset(BuildContext context) async {
    final _data = await DefaultAssetBundle.of(context)
        .loadString('assets/files/recetas.txt');
    setState(() {
      _content = _data;
      data = _content.split("Titulo:");
      data2 = data[numH].toString().split("Grafico: $numH");
    });
  }

  /*Future<String> _getDirPath() async {
    final _dir = await getApplicationDocumentsDirectory();
    return _dir.path;
  }

  Future<void> _readData() async {
    final _dirPath = await _getDirPath();
    final _myFile = File('$_dirPath/recetas.txt');
    final _data = await _myFile.readAsString(encoding: utf8);
    setState(() {
      _content = _data;
    });
  }*/

  @override
  Widget build(BuildContext context) {
    _loadAsset(context);
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          toolbarHeight: 100,
          backgroundColor: Colors.black,
          title: Image.asset(
            "assets/images/icoH.png",
            height: 80,
            width: MediaQuery.of(context).size.width / 1.5,
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Colors.black,
                    Colors.red,
                    Colors.orange.withOpacity(0.8),
                    Colors.yellow.withOpacity(0.8)
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomRight,
                  stops: [0.15, 0.3, 0.6, 0.8])),
          child: Center(
              child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                child: Image.asset(
                  "assets/images/${image[numH - 1]}.jpg",
                  height: MediaQuery.of(context).size.height / 2.5,
                  width: MediaQuery.of(context).size.width / 1.5,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                _content != null ? data2[0].toString() : 'Nothing to show',
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                textAlign: TextAlign.center,
                textDirection: TextDirection.ltr,
              ),
              Text(
                _content != null ? data2[1].toString() : 'Nothing to show',
                textAlign: TextAlign.justify,
              ),
            ],
          )),
        )));
  }
}
