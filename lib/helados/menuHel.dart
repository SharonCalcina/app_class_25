import 'package:app_class_25/helados/helados.dart';
import 'package:flutter/material.dart';

class MenuHel extends StatefulWidget {
  @override
  _MenuHelState createState() => _MenuHelState();
}

class _MenuHelState extends State<MenuHel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Image.asset(
            "assets/images/hel2.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Center(
            child: Column(
              children: [
                SizedBox(
                  height: 100,
                ),
                Text(
                  "Recetas de Helados",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 35,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                ),
                /*Image.asset(
                  "assets/images/im0.png",
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),*/
                button("Helado de yogur, chocolate y brownie", Helados(1)),
                SizedBox(
                  height: 5,
                ),
                button(
                    "Helado de Vainilla y galletas Oreo, chocolate y brownie",
                    Helados(2)),
                SizedBox(
                  height: 5,
                ),
                button("Sandwich de helado casero con galletas de chocolate",
                    Helados(3)),
                SizedBox(
                  height: 5,
                ),
                button("Helado de plátano, nueves y chocolate", Helados(4)),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          )
        ],
      )),
    );
  }

  Widget button(title, page) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.white.withOpacity(0.2),
          padding: EdgeInsets.all(12)),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return page;
        }));
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
