import 'dart:convert';
import 'dart:io';

import 'package:app_class_25/planificacion/homePlan.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class Actualizar extends StatefulWidget {
  final String dat;
  final int index;
  Actualizar(this.dat, this.index);
  @override
  _ActualizarState createState() => _ActualizarState(this.dat, this.index);
}

class _ActualizarState extends State<Actualizar> {
  String hint = "Pendiente";
  _ActualizarState(this.dat, this.index);
  String dat;
  int index;
  int nroC = 0;
  String readRes = "";
  StringBuffer contents = StringBuffer();

  List data = [];
  List estados = [];
  List<String> dias = [];
  List<String> horas = [];
  List<String> fechas = [];
  List<String> resumenes = [];
  List<String> observaciones = [];
  String f = "";
  List<String> est = [];
  List<String> dia = [];
  List<String> hora = [];
  List<String> fecha = [];
  List<String> resumen = [];
  List<String> observacion = [];
  TextEditingController asunto1 = TextEditingController();
  TextEditingController dia1 = TextEditingController();
  TextEditingController hora1 = TextEditingController();
  TextEditingController fecha1 = TextEditingController();
  TextEditingController resumen1 = TextEditingController();
  TextEditingController observacion1 = TextEditingController();
  _busqueda() {
    List<String> data22 = dat.split("ASUNTO:");

    print(data22.toString());
    print(data22.length);

    List<String> jump = [];
    for (var i = 1; i < data22.length; i++) {
      jump.add(data22[i].split("Dia")[0].toString());
      dia.add(data22[i].split("Dia:")[1].split("Hora")[0].toString());
      fecha.add(data22[i].split("Fecha:")[1].split("Estado")[0].toString());
      hora.add(data22[i].split("Hora:")[1].split("Fecha")[0].toString());
      est.add(data22[i].split("Estado:")[1].split("Resumen")[0].toString());
      resumen.add(
          data22[i].split("Resumen:")[1].split("Observacion")[0].toString());
      observacion.add(data22[i].split("Observacion:")[1].toString());
    }
    print(est.toString());
    print(dia.toString());
    print(hora.toString());
    print(fecha.toString());
    print(resumen.toString());
    print(observacion.toString());

    setState(() {
      data = jump;
      nroC = data.length;
      dias = dia;
      horas = hora;
      fechas = fecha;
      estados = est;
      resumenes = resumen;
      observaciones = observacion;
      asunto1 = TextEditingController(text: " ${data[index].toString()}");
      dia1 = TextEditingController(text: "${dias[index]}");
      hora1 = TextEditingController(text: "${horas[index]}");
      fecha1 = TextEditingController(text: "${fechas[index]}");
      resumen1 = TextEditingController(
          text: "${resumenes[index].toString().replaceAll("\n", "")}");
      observacion1 = TextEditingController(
          text: "${observaciones[index].toString().replaceAll("\n", "")}");
      if (est[index].contains("x")) {
        hint = "Cancelada";
      } else if (est[index].contains("Ok")) {
        hint = "Realizada";
      } else if (est[index].contains(" ")) {
        hint = "Proxima";
      } else if (est[index].contains("P")) {
        hint = "Pendiente";
      }
    });
  }

  _createdoc(int len, asunto, dia, hora, fecha, estado, resumen, obs) {
    String newdata = "";
    for (var i = 0; i < len; i++) {
      if (i == index) {
        String newhint = "";
        if (hint == "Pendiente") {
          newhint = "P";
        } else if (hint == "Cancelada") {
          newhint = "x";
        } else if (hint == "Realizada") {
          newhint = "Ok";
        } else if (hint == "Proxima") {
          newhint = " ";
        }
        newdata = newdata +
            "\n" +
            "ASUNTO: " +
            asunto1.value.text.toString() +
            "\n" +
            "Dia: " +
            dia1.value.text.toString() +
            "\n" +
            "Hora: " +
            hora1.value.text.toString() +
            "\n" +
            "Fecha: " +
            fecha1.value.text.toString() +
            "Estado: " +
            newhint +
            "\n" +
            "Resumen: " +
            resumen1.value.text.toString() +
            "\n" +
            "Observacion: " +
            observacion1.value.text.toString();
      } else {
        newdata = newdata +
            "\n" +
            "ASUNTO: " +
            asunto[i].toString() +
            "\n" +
            "Dia: " +
            dia[i].toString() +
            "\n" +
            "Hora: " +
            hora[i].toString() +
            "\n" +
            "Fecha: " +
            fecha[i].toString() +
            "Estado: " +
            estado[i].toString() +
            "\n" +
            "Resumen: " +
            resumen[i].toString() +
            "\n" +
            "Observacion: " +
            obs[i].toString();
      }
    }
    _readFile(newdata);
  }

  String asuntos1 = "";
  String dias1 = "";
  String horas1 = "";
  String fechas1 = "";
  String resumenes1 = "";
  String observaciones1 = "";
  StringBuffer contents1 = StringBuffer();
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  String done = "";
  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/planifica.txt');
  }

  Future<String> _readFile(String text) async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        print("--------------rrrrrrrrrrrrrr------------");

        @override
        IOSink openWrite({
          FileMode mode = FileMode.writeOnly,
          Encoding encoding = utf8,
        }) {
          var contentStream = file.openWrite(mode: mode, encoding: encoding);
          contentStream.write(text);
          contentStream.close();
          return contentStream;
        }

        openWrite();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  @override
  void initState() {
    setState(() {
      _busqueda();
      f = this.dat;
    });

    super.initState();
  }

  String val = "";
  final key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    //_busqueda();

    double w = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          actions: [
            Container(
                margin: EdgeInsets.only(right: 10),
                child: Image.asset(
                  "assets/images/plann.png",
                  height: 20,
                  width: 50,
                ))
          ],
          centerTitle: true,
          title: Image.asset(
            "assets/images/sistema.png",
            width: 200,
          ),
          backgroundColor: Colors.transparent,
          elevation: 500,
          shadowColor: Colors.white,
        ),
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(15),
                child: Form(
                  key: key,
                  child: Column(children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "NUEVO ASUNTO",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    fields("Ingrese el Asunto", 'Error. Ingrese un Asunto',
                        asunto1, asuntos1, w / 1.2),
                    fields("Ingrese el Fecha", 'Error. Ingrese una Fecha',
                        fecha1, fechas1, w / 1.2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        fields("Ingrese el Dia", 'Error. Ingrese un dia', dia1,
                            dias1, w / 2.5),
                        fields("Ingrese la Hora", 'Error. Ingrese una hora',
                            hora1, horas1, w / 2.5),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "Estado: ",
                          style: TextStyle(color: Colors.amber),
                        ),
                        DropdownButton(
                            style: TextStyle(color: Colors.white, fontSize: 15),
                            dropdownColor: Colors.black,
                            value: hint,
                            items: [
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(
                                        Icons.check_box_outline_blank_outlined),
                                    Text("Pendiente"),
                                  ],
                                ),
                                value: "Pendiente",
                              ),
                              DropdownMenuItem(
                                child: Row(children: [
                                  Icon(Icons.close),
                                  Text("Cancelada"),
                                ]),
                                value: "Cancelada",
                                onTap: () {},
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(Icons.check_box_outlined),
                                    Text("Realizada"),
                                  ],
                                ),
                                value: "Realizada",
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(Icons.remove_red_eye_outlined),
                                    Text("Proxima"),
                                  ],
                                ),
                                value: "Proxima",
                              ),
                            ],
                            onChanged: (String value) {
                              setState(() {
                                hint = value;
                              });
                            })
                      ],
                    ),
                    fields("Ingrese el Resumen", 'Error. Ingrese un Resúmen',
                        resumen1, resumenes1, w / 1.2),
                    fields(
                        "Ingrese la Observacion",
                        'Error. Ingrese una Observación',
                        observacion1,
                        observaciones1,
                        w / 1.2),
                    SizedBox(
                      height: 10,
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor:
                              Colors.green.shade800.withOpacity(0.5),
                          padding: EdgeInsets.all(12)),
                      onPressed: () {
                        if (key.currentState.validate()) {
                          _createdoc(nroC, data, dias, horas, fechas, estados,
                              resumenes, observaciones);
                          HomePlan(dat);
                        }
                      },
                      child: Text(
                        "GUARDAR",
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.8), fontSize: 15),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    )
                  ]),
                ))));
  }

  /* Widget button(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.green.shade800.withOpacity(0.5),
          padding: EdgeInsets.all(12)),
      onPressed: () {
        _readFile("\n" +
            "ASUNTO: " +
            asunto1.value.text.toString() +
            "\n" +
            "Dia: " +
            fecha1.value.text.toString() +
            "\n" +
            "Hora: " +
            dia1.value.text.toString() +
            "\n" +
            "Fecha: " +
            hora1.value.text.toString() +
            "Estado: " +
            "x" +
            "\n" +
            "Resumen: " +
            resumen1.value.text.toString() +
            "\n" +
            "Observacion: " +
            observacion1.value.text.toString());
      },
      child: Text(
        title,
        style: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 15),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }*/

  Widget fields(label, error, controlador, variable, size) {
    return Container(
        width: size,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.2),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.only(bottom: 2),
        child: TextFormField(
          //initialValue: "hola",
          //keyboardType: TextInputType.number,
          validator: (text) {
            if (text == null || text.isEmpty) {
              return error;
            }
            return null;
          },
          style: TextStyle(color: Colors.white),
          controller: controlador,
          onChanged: (value) {
            setState(() {
              variable = value;
            });
          },
          decoration: InputDecoration(
            errorStyle: TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
            labelText: label,
            labelStyle: TextStyle(
                fontSize: 13.5, color: Colors.amber.shade800.withOpacity(0.6)),
          ),
        ));
  }
}
