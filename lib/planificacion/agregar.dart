import 'dart:convert';
import 'dart:io';

import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class Agregat extends StatefulWidget {
  @override
  _AgregatState createState() => _AgregatState();
}

class _AgregatState extends State<Agregat> {
  String hint = "Pendiente";
  final key = new GlobalKey<FormState>();
  TextEditingController asunto = TextEditingController();
  TextEditingController dia = TextEditingController();
  TextEditingController hora = TextEditingController();
  TextEditingController fecha = TextEditingController();
  TextEditingController resumen = TextEditingController();
  TextEditingController observacion = TextEditingController();
  String asuntos = "";
  String dias = "";
  String horas = "";
  String fechas = "";
  String resumenes = "";
  String observaciones = "";
  StringBuffer contents = StringBuffer();
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  String done = "";
  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/planifica.txt');
  }

  Future<String> _readFile(String text) async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        print("--------------rrrrrrrrrrrrrr------------");
        /*setState(() async {
          Encoding encoding = utf8;
          //readRes = await file.readAsString();
          var contentStream =
              await file.openWrite({FileMode.writeOnly, encoding});
          await contentStream.write("hwllo\n");
          contentStream.close();
        });*/
        @override
        IOSink openWrite({
          FileMode mode = FileMode.writeOnlyAppend,
          Encoding encoding = utf8,
        }) {
          var contentStream = file.openWrite(mode: mode, encoding: encoding);
          contentStream.write(text);
          contentStream.close();
          return contentStream;
        }

        openWrite();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          actions: [
            Container(
                margin: EdgeInsets.only(right: 10),
                child: Image.asset(
                  "assets/images/plann.png",
                  height: 20,
                  width: 50,
                ))
          ],
          centerTitle: true,
          title: Image.asset(
            "assets/images/sistema.png",
            width: 200,
          ),
          backgroundColor: Colors.transparent,
          elevation: 500,
          shadowColor: Colors.white,
        ),
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(15),
                child: Form(
                  key: key,
                  child: Column(children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "NUEVO ASUNTO",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    fields("Ingrese el Asunto", 'Error. Ingrese un Asunto',
                        asunto, asuntos, w / 1.2),
                    fields("Ingrese el Fecha", 'Error. Ingrese una Fecha',
                        fecha, fechas, w / 1.2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        fields("Ingrese el Dia", 'Error. Ingrese un dia', dia,
                            dias, w / 2.5),
                        fields("Ingrese la Hora", 'Error. Ingrese una hora',
                            hora, horas, w / 2.5),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "Estado: ",
                          style: TextStyle(color: Colors.amber),
                        ),
                        DropdownButton(
                            style: TextStyle(color: Colors.white, fontSize: 15),
                            dropdownColor: Colors.black,
                            value: hint,
                            items: [
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(
                                        Icons.check_box_outline_blank_outlined),
                                    Text("Pendiente"),
                                  ],
                                ),
                                value: "Pendiente",
                              ),
                              DropdownMenuItem(
                                child: Row(children: [
                                  Icon(Icons.close),
                                  Text("Cancelada"),
                                ]),
                                value: "Cancelada",
                                onTap: () {},
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(Icons.check_box_outlined),
                                    Text("Realizada"),
                                  ],
                                ),
                                value: "Realizada",
                              ),
                              DropdownMenuItem(
                                child: Row(
                                  children: [
                                    Icon(Icons.remove_red_eye_outlined),
                                    Text("Proxima"),
                                  ],
                                ),
                                value: "Proxima",
                              ),
                            ],
                            onChanged: (String value) {
                              setState(() {
                                hint = value;
                              });
                            })
                      ],
                    ),
                    fields("Ingrese el Resumen", 'Error. Ingrese un Resúmen',
                        resumen, resumenes, w / 1.2),
                    fields(
                        "Ingrese la Observacion",
                        'Error. Ingrese una Observación',
                        observacion,
                        observaciones,
                        w / 1.2),
                    SizedBox(
                      height: 10,
                    ),
                    button("GUARDAR"),
                  ]),
                ))));
  }

  Widget button(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.green.shade800.withOpacity(0.5),
          padding: EdgeInsets.all(12)),
      onPressed: () {
        if (key.currentState.validate()) {
          String newhint = "";
          if (hint == "Pendiente") {
            newhint = "P";
          } else if (hint == "Cancelada") {
            newhint = "x";
          } else if (hint == "Realizada") {
            newhint = "Ok";
          } else if (hint == "Proxima") {
            newhint = " ";
          }
          _readFile("\n" +
              "ASUNTO: " +
              asunto.value.text.toString() +
              "\n" +
              "Dia: " +
              dia.value.text.toString() +
              "\n" +
              "Hora: " +
              hora.value.text.toString() +
              "\n" +
              "Fecha: " +
              fecha.value.text.toString() +
              "Estado: " +
              newhint +
              "\n" +
              "Resumen: " +
              resumen.value.text.toString() +
              "\n" +
              "Observacion: " +
              observacion.value.text.toString());
        }
      },
      child: Text(
        title,
        style: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 15),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }

  Widget fields(label, error, controlador, variable, size) {
    return Container(
        width: size,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.2),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        margin: EdgeInsets.only(bottom: 2),
        child: TextFormField(
          //initialValue: "hola",
          //keyboardType: TextInputType.number,
          validator: (text) {
            if (text == null || text.isEmpty) {
              return error;
            }
            return null;
          },
          style: TextStyle(color: Colors.white),
          controller: controlador,
          onChanged: (value) {
            setState(() {
              variable = value;
            });
          },
          decoration: InputDecoration(
            errorStyle: TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
            labelText: label,
            labelStyle: TextStyle(
                fontSize: 13.5, color: Colors.amber.shade800.withOpacity(0.6)),
          ),
        ));
  }
}
