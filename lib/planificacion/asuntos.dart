import 'package:flutter/material.dart';

class Asuntos extends StatefulWidget {
  final String dat;
  final int index;
  Asuntos(this.dat, this.index);
  @override
  _AsuntosState createState() => _AsuntosState(this.dat, this.index);
}

class _AsuntosState extends State<Asuntos> {
  _AsuntosState(this.dat, this.index);
  String dat;
  int index;
  int nroC = 0;
  String readRes = "";
  StringBuffer contents = StringBuffer();

  String nombre = "Sin Resultado";
  String cuentas = "Sin Saldo";
  String profesion = "Sin resultado";
  String prof = "Sin resultado";
  List data = [];
  List estados = [];
  List<String> dias = [];
  List<String> horas = [];
  List<String> fechas = [];
  List<String> resumenes = [];
  List<String> observaciones = [];
  String f = "";
  List<String> est = [];
  List<String> dia = [];
  List<String> hora = [];
  List<String> fecha = [];
  List<String> resumen = [];
  List<String> observacion = [];
  _busqueda() {
    List<String> data22 = dat.split("ASUNTO:");

    print(data22.toString());
    print(data22.length);

    List<String> jump = [];
    for (var i = 1; i < data22.length; i++) {
      jump.add(data22[i].split("Dia")[0].toString());
      dia.add(data22[i].split("Dia:")[1].split("Hora")[0].toString());
      fecha.add(data22[i].split("Fecha:")[1].split("Estado")[0].toString());
      hora.add(data22[i].split("Hora:")[1].split("Fecha")[0].toString());
      est.add(data22[i].split("Estado:")[1].split("Resumen")[0].toString());
      resumen.add(
          data22[i].split("Resumen:")[1].split("Observacion")[0].toString());
      observacion.add(data22[i].split("Observacion:")[1].toString());
    }
    print(est.toString());
    print(dia.toString());
    print(hora.toString());
    print(fecha.toString());
    print(resumen.toString());
    print(observacion.toString());

    setState(() {
      data = jump;
      nroC = data.length;
      dias = dia;
      horas = hora;
      fechas = fecha;
      estados = est;
      resumenes = resumen;
      observaciones = observacion;
    });
  }

  @override
  void initState() {
    setState(() {
      _busqueda();
      f = this.dat;
    });

    super.initState();
  }

  TextEditingController departamento = new TextEditingController();

  String val = "";
  final key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    _busqueda();
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        actions: [
          Container(
              margin: EdgeInsets.only(right: 10),
              child: Image.asset(
                "assets/images/plann.png",
                height: 20,
                width: 50,
              ))
        ],
        centerTitle: true,
        title: Image.asset(
          "assets/images/sistema.png",
          width: 200,
        ),
        backgroundColor: Colors.transparent,
        elevation: 500,
        shadowColor: Colors.white,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: 50,
            ),
          ),
          SliverToBoxAdapter(
              child: Center(
                  child: text2(
                      "ASUNTO:   ${data[index].toString().toUpperCase()}"))),
          SliverToBoxAdapter(
              child: Center(child: text("Fecha:${fechas[index]}"))),
          SliverToBoxAdapter(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              text("Hora:${horas[index]}"),
              text("Dia:${dias[index]}")
            ],
          )),
          SliverToBoxAdapter(
              child: Center(
                  child: Container(
            width: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.white.withOpacity(0.2),
            ),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: text(
                "Resumen:\n${resumenes[index].toString().replaceAll("\n", "")}"),
          ))),
          SliverToBoxAdapter(
              child: Center(
                  child: Container(
                      width: 300,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Colors.white.withOpacity(0.2),
                      ),
                      child: text(
                          "Observacion:\n${observaciones[index].toString().replaceAll("\n", "")}")))),
        ],
      ),
    );
  }

  Widget text(title) {
    return Text(
      title,
      style: TextStyle(color: Colors.white, fontSize: 15),
      textAlign: TextAlign.justify,
    );
  }

  Widget text2(title) {
    return Text(
      title,
      style: TextStyle(
          color: Colors.blue, fontSize: 16, fontWeight: FontWeight.bold),
      textAlign: TextAlign.justify,
    );
  }
}
