import 'dart:convert';
import 'dart:io';

import 'package:app_class_25/planificacion/actualizar.dart';
import 'package:app_class_25/planificacion/agregar.dart';
import 'package:app_class_25/planificacion/asuntos.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePlan extends StatefulWidget {
  final String dat;
  HomePlan(this.dat);
  @override
  _HomePlanState createState() => _HomePlanState(this.dat);
}

class _HomePlanState extends State<HomePlan> {
  _HomePlanState(this.dat);
  int index = 0;
  String dat;
  int nroC = 0;
  String readRes = "";
  StringBuffer contents = StringBuffer();

  String hint = "Todos";

  String nombre = "Sin Resultado";
  String cuentas = "Sin Saldo";
  String profesion = "Sin resultado";
  String prof = "Sin resultado";
  List data = [];
  List data2 = [];
  List estados = [];
  List estados2 = [];
  String f = "";

  _busqueda() {
    List<String> data22 = dat.split("ASUNTO:");
    List<String> est = [];
    print(data22.toString());
    print(data22.length);

    List<String> jump = [];
    for (var i = 1; i < data22.length; i++) {
      jump.add(data22[i].split("Dia")[0].toString());
      est.add(data22[i].split("Estado:")[1].split("Resumen")[0].toString());
    }
    print(est.toString());

    setState(() {
      data = jump;
      data2 = jump;
      nroC = data.length;
      estados = est;
      estados2 = est;
    });
  }

  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/planifica.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        print("--------------rrrrrrrrrrrrrr------------");
        setState(() async {
          //readRes = await file.readAsString();
          var contentStream = file.openRead();

          contentStream
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents.write(line + "\n"),
                  onDone: () => readRes = contents.toString(),
                  onError: (e) => print('[Problems]: $e'));
        });
        print(readRes.toString());
        return await file.openRead();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  _busqueda2() {
    List<String> data22 = readRes.split("ASUNTO:");
    print(data22.toString());
    print(data22.length);
    List<String> jump = [];
    for (var i = 1; i < data22.length; i++) {
      jump.add(data22[i].split("Dia")[0].toString());
    }

    setState(() {
      data = jump;
      data2 = jump;
    });
  }

  @override
  void initState() {
    _readFile();
    setState(() {
      _busqueda();
      f = this.dat;
    });

    super.initState();
  }

  TextEditingController departamento = new TextEditingController();

  String val = "";
  final key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    // _busqueda();
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          actions: [
            Container(
                margin: EdgeInsets.only(right: 10),
                child: Image.asset(
                  "assets/images/plann.png",
                  height: 20,
                  width: 50,
                ))
          ],
          centerTitle: true,
          title: Image.asset(
            "assets/images/sistema.png",
            width: 200,
          ),
          backgroundColor: Colors.transparent,
          elevation: 500,
          shadowColor: Colors.white,
        ),
        body: CustomScrollView(slivers: [
          SliverToBoxAdapter(
              child: SizedBox(
            height: 30,
          )),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Filtro Tareas: ",
                  style: TextStyle(color: Colors.amber),
                ),
                DropdownButton(
                    style: TextStyle(color: Colors.white, fontSize: 15),
                    dropdownColor: Colors.black,
                    value: hint,
                    items: [
                      DropdownMenuItem(
                        child: Row(
                          children: [
                            Icon(Icons.check_box_outline_blank_outlined),
                            Text("Todos"),
                          ],
                        ),
                        value: "Todos",
                        onTap: () {
                          setState(() {
                            data = data2;
                            estados = estados2;
                          });
                        },
                      ),
                      DropdownMenuItem(
                        child: Row(
                          children: [
                            Icon(Icons.check_box_outline_blank_outlined),
                            Text("Pendiente"),
                          ],
                        ),
                        value: "Pendiente",
                        onTap: () {
                          setState(() {
                            List data3 = [];
                            List estado3 = [];
                            for (var i = 0; i < data2.length; i++) {
                              if (estados2[i].toString().contains("P")) {
                                data3.add(data2[i]);
                                estado3.add(estados2[i]);
                              }
                            }
                            data = data3;
                            estados = estado3;
                          });
                        },
                      ),
                      DropdownMenuItem(
                        child: Row(children: [
                          Icon(Icons.close),
                          Text("Cancelada"),
                        ]),
                        value: "Cancelada",
                        onTap: () {
                          setState(() {
                            List data3 = [];
                            List estado3 = [];
                            for (var i = 0; i < data2.length; i++) {
                              if (estados2[i].toString().contains("x")) {
                                data3.add(data2[i]);
                                estado3.add(estados2[i]);
                              }
                            }
                            data = data3;
                            estados = estado3;
                            print("llllllllrn${data.length}");
                          });
                        },
                      ),
                      DropdownMenuItem(
                        child: Row(
                          children: [
                            Icon(Icons.check_box_outlined),
                            Text("Realizada"),
                          ],
                        ),
                        value: "Realizada",
                        onTap: () {
                          setState(() {
                            List data3 = [];
                            List estado3 = [];
                            for (var i = 0; i < data2.length; i++) {
                              if (estados2[i].toString().contains("Ok")) {
                                data3.add(data2[i]);
                                estado3.add(estados2[i]);
                              }
                            }
                            data = data3;
                            estados = estado3;
                          });
                        },
                      ),
                      DropdownMenuItem(
                        child: Row(
                          children: [
                            Icon(Icons.remove_red_eye_outlined),
                            Text("Proxima"),
                          ],
                        ),
                        value: "Proxima",
                        onTap: () {
                          setState(() {
                            List data3 = [];
                            List estado3 = [];
                            for (var i = 0; i < data2.length; i++) {
                              if (!estados2[i].toString().contains("P") &&
                                  !estados2[i].toString().contains("Ok") &&
                                  !estados2[i].toString().contains("x")) {
                                data3.add(data2[i]);
                                estado3.add(estados2[i]);
                              }
                            }
                            data = data3;
                            estados = estado3;
                          });
                        },
                      ),
                    ],
                    onChanged: (String value) {
                      setState(() {
                        hint = value;
                      });
                    }),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 25,
            ),
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate((BuildContext context, e) {
            return ListTile(
              leading: IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.cyan.withOpacity(0.8),
                  ),
                  onPressed: () {
                    setState(() {
                      index = e;
                    });
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Actualizar(dat, index);
                    }));
                  }),
              selected: true,
              title: text(data[e]),
              tileColor: Colors.amber,
              trailing: (estados[e].toString().contains("x"))
                  ? Icon(
                      Icons.close,
                      color: Colors.red,
                    )
                  : (estados[e].toString().contains("Ok"))
                      ? Icon(
                          Icons.check_box_outlined,
                          color: Colors.green,
                        )
                      : (estados[e].toString().contains("P"))
                          ? Icon(
                              Icons.check_box_outline_blank,
                              color: Colors.white,
                            )
                          : Icon(
                              Icons.remove_red_eye_outlined,
                              color: Colors.amber,
                            ),
              focusColor: Colors.red,
              selectedTileColor: Colors.black,
              onTap: () {
                setState(() {
                  index = e;
                });
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Asuntos(dat, index);
                }));
              },
            );
          }, childCount: data.length)),
        ]),
        bottomNavigationBar: Container(
            height: 80,
            color: Colors.white.withOpacity(0.2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [button2("ACTUALIZAR"), button("AGREGAR")],
            )));
  }

  Widget text(title) {
    return Text(
      title,
      style: TextStyle(color: Colors.white, fontSize: 15),
    );
  }

  Widget button2(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.cyan.shade800,
          padding: EdgeInsets.all(12)),
      onPressed: () {
        _busqueda2();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return HomePlan(readRes.toString());
        }));
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }

  Widget button(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.cyan.shade800,
          padding: EdgeInsets.all(12)),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return Agregat();
        }));
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
