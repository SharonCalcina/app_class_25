import 'dart:convert';
import 'dart:io';

import 'package:app_class_25/planificacion/homePlan.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class MainPlan extends StatefulWidget {
  @override
  _MainPlanState createState() => _MainPlanState();
}

class _MainPlanState extends State<MainPlan> {
  String readRes = "";
  StringBuffer contents = StringBuffer();
  Future _getPath() async {
    String pathDownload = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOWNLOADS);
    print(pathDownload);
    String pathDoc = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);
    print(pathDoc);
    return pathDownload;
  }

  Future _getFile() async {
    String file = await _getPath();
    return file;
  }

  Future get _localFile async {
    final path = await _getFile();
    print(path);
    return File('$path/planifica.txt');
  }

  Future _readFile() async {
    final status = await Permission.storage.request();
    if (status == PermissionStatus.granted) {
      try {
        final file = await _localFile;
        print("--------------rrrrrrrrrrrrrr------------");
        setState(() async {
          //readRes = await file.readAsString();
          var contentStream = file.openRead();

          contentStream
              .transform(Utf8Decoder())
              .transform(LineSplitter())
              .listen((String line) => contents.write(line + "\n"),
                  onDone: () => readRes = contents.toString(),
                  onError: (e) => print('[Problems]: $e'));
        });
        print(readRes.toString());
        return await file.openRead();
      } catch (e) {
        return e.toString();
      }
    } else {
      openAppSettings();
    }
  }

  List data = [];
  _busqueda() {
    List<String> data22 = readRes.split("ASUNTO:");
    print(data22.toString());
    print(data22.length);
    List<String> jump = [];
    for (var i = 1; i < data22.length; i++) {
      jump.add(data22[i].split("Dia")[0].toString());
    }

    setState(() {
      data = jump;
    });
  }

  @override
  void initState() {
    super.initState();
    _readFile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Opacity(
              opacity: 0.5,
              child: Image.asset(
                "assets/images/plan.jpg",
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              )),
          Center(
            child: Column(
              children: [
                SizedBox(
                  height: 100,
                ),
                Text(
                  "Sistema de Planificacion de Personal",
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 45),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 2,
                ),
                button("Menu Principal", context),
                SizedBox(
                  height: 10,
                ),
                button2("Finalizar"),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          )
        ],
      )),
    );
  }

  Widget button(title, context) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.red.shade900.withOpacity(0.5),
          padding: EdgeInsets.all(12)),
      onPressed: () {
        _busqueda();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return HomePlan(readRes.toString());
        }));
      },
      child: Text(
        title,
        style: TextStyle(color: Colors.white, fontSize: 16),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }

  Widget button2(title) {
    return TextButton(
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          backgroundColor: Colors.black.withOpacity(0.5),
          padding: EdgeInsets.all(12)),
      onPressed: () {
        SystemNavigator.pop();
      },
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
        ),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
    );
  }
}
